<?php
/**
 * @version		$Id: default_head.php 36 2011-03-30 15:24:58Z bfoecke $
 * @package		Goodpractice
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
?>
<tr>
    <th min-width="400px">
        <?php echo JText::_('JGLOBAL_TITLE'); ?>
    </th>
    <th min-width="80px">
        <?php echo JText::_('Start'); ?>
    </th>
    <th min-width="80px">
        <?php echo JText::_('Ende'); ?>
    </th>
    <th min-width="220px">
        <?php // echo JText::_('COM_GOODPRACTICE_ENTRY_CATEGORY');  ?>
        <?php echo JText::_('Schlagworte'); ?>
    </th>
    <th>
        <?php echo JText::_('Tr&auml;ger'); ?>
    </th>
    <th>
        <?php echo JText::_('Hochschule'); ?>
    </th>
    <th>
        <?php echo JText::_('Region'); ?>
    </th>
</tr>

