<?php
/**
 * @version		$Id: default_foot.php 9 2011-03-22 14:55:21Z bfoecke $
 * @package		Goodpractice
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted Access');
?>
<tr>
    <td colspan="20"><?php echo $this->pagination->getListFooter(); ?></td>
</tr>
