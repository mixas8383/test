<?php
/**
 * @version		$Id: default.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Goodpractice
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

// Get access view levels
$user = JFactory::getUser();
$access = $user->getAuthorisedViewLevels();

// Get current menu item
$menu = JFactory::getApplication()->getMenu();
$active = $menu->getActive();
if ($active)
{
    $itemId = $active->id;
} else
{
    $itemId = 0;
}

// Create shortcuts to parameters.
$params = $this->params;

// Compute selected asset permissions.
$user = JFactory::getUser();
$userId = $user->get('id');
$asset = 'com_goodpractice.entry.' . $this->item->id;

// Compute some flags
$showPrint = $params->get('show_print_icon', 1);
$showEmail = $params->get('show_email_icon', 1);
// $showEdit		= $user->authorise('core.edit', $asset) || !empty($userId) && $user->authorise('core.edit.own', $asset) && $userId == $this->item->created_by;
$showCategory = $params->get('show_category', 1) && $this->item->catid;
$showLanguage = $params->get('show_language', 1);
$showCreate = $params->get('show_create_date', 1);
$showModify = $params->get('show_modify_date', 1);
$showHits = $params->get('show_hits', 1);
$showVariants = $params->get('show_variants', 1) && (count($this->item->tags) > 1 || (isset($this->item->tags[0]) && $this->item->tags[0] != $this->item->title));
$showPdfs = $params->get('show_pdfs', 1) && (count($this->item->pdfs) >= 1 );
$showLinks = $params->get('show_links', 1) && (count($this->item->links) >= 1 );
$entryID = $this->item->id;
?>
<div id="goodpractice" class="goodpractice-entry<?php echo $params->get('pageclass_sfx') ?>">
    <?php $contactDataArray = GoodpracticeHelperRoute::getContactData($entryID); ?>

    <!-- �berschrift ausgeblendet, auf Wunsch von Karsten Herrmann (27.07.2016) -->
    <?php if ($params->get('show_page_heading', 1)): ?>
        <h1><?php // echo $this->escape($params->get('page_heading'));       ?></h1>
    <?php endif; ?>

    <?php if ($contactDataArray['id']): ?>
        <?php if ($contactDataArray['image']): ?>
            <div style="width:100%; height:50px">
                <div style="float:right"><a href='http://www.kitaundco.de/index.php?option=com_contact&view=contact&layout=modal&id=<?= $contactDataArray['id'] ?>' data-fb-options="width:600px;"><img align="right" height='50px' src='http://www.kitaundco.de/<?= $contactDataArray['image'] ?>' title='<?= $contactDataArray['name'] ?>' alt='<?= $contactDataArray['name'] ?>'/></a><br /><br /><br /><a href='http://www.kitaundco.de/index.php?option=com_contact&view=contact&layout=modal&id=<?= $contactDataArray['id'] ?>' data-fb-options="width:600px;"><?php echo $contactDataArray['name']; ?></a></div>
            </div>
            <? else: ?>
            <div style="width:100%; height:30px">
                <div style="float:right"><p><a href='http://www.kitaundco.de/index.php?option=com_contact&view=contact&layout=modal&id=<?= $contactDataArray['id'] ?>' data-fb-options="width:600px;"><?php echo $contactDataArray['name']; ?></a></p></div>
            </div>
        <?php endif; ?>
    <?php endif; ?>

    <?php if ($params->get('show_title', 1)): ?>
        <h1 style="line-height:1.3em; text-align: left"><?php echo $this->escape($this->item->title); ?></h1>
        <?php if (isset($this->item->subtitle) && !empty($this->item->subtitle)) : ?>
            <h2 style="text-align: left"><?php echo $this->escape($this->item->subtitle); ?></h2>
        <?php endif; ?>
    <?php endif; ?>

    <?php if ($showCategory || $showCreate || $showModify || $showHit || $showVariants || $showLanguage) : ?>
        <dl class="entry-info">
            <dt class="entry-info-term"><?php //echo JText::_('COM_GOODPRACTICE_ENTRY_INFO');         ?></dt>

            <?php if ($showCategory): ?>
                <dd class="category-name">
                    <?php echo JText::_('COM_GOODPRACTICE_ENTRY_CATEGORY'); ?>
                    <?php if ($params->get('link_category', 1) && ($params->get('show_noauth', 0) || in_array($this->item->category->access, $access))): ?>
                        <a href="<?php echo JRoute::_(GoodpracticeHelperRoute::getCategoryRoute($this->item->category->slug)); ?>"><?php echo $this->escape($this->item->category->title); ?></a>
                    <?php else: ?>
                        <?php echo $this->escape($this->item->category->title); ?>
                    <?php endif; ?>
                </dd>
            <?php endif; ?>

            <?php if ($showLanguage) : ?>
                <dd class="language">
                    <?php echo JText::_('COM_GOODPRACTICE_ENTRY_LANGUAGE'); ?>
                    <?php echo $this->escape($this->item->language_title); ?>
                </dd>
            <?php endif; ?>

            <?php if ($showCreate) : ?>
                <dd class="create"><?php echo JText::sprintf('COM_GOODPRACTICE_ENTRY_CREATED_DATE_ON', JHtml::_('date', $this->item->created, JText::_('DATE_FORMAT_LC2')), $this->item->created_by_alias ? $this->item->created_by_alias : $this->item->author); ?></dd>
            <?php endif; ?>

            <?php if ($showModify) : ?>
                <dd class="modified"><?php echo JText::sprintf('COM_GOODPRACTICE_ENTRY_LAST_UPDATED', JHtml::_('date', $this->item->modified, JText::_('DATE_FORMAT_LC2')), $this->item->created_by_alias ? $this->item->created_by_alias : $this->item->author); ?></dd>
            <?php endif; ?>

            <?php if ($showHits) : ?>
                <dd class="hits"><?php echo JText::sprintf('COM_GOODPRACTICE_ENTRY_HITS', $this->item->hits); ?></dd>
            <?php endif; ?>

        </dl>
    <?php endif; ?>

    <?php if (isset($this->item->toc)) : ?>
        <?php echo $this->item->toc; ?>
    <?php endif; ?>

    <?php if (in_array($this->item->access, $access)): ?>
        <?php
        if ($this->item->text2 != "")
        {
            ?>
            <div id="jsapp-files" class="app-box">
                <div class="app-box-header">
                    <div class="app-box-header">
                        <h2 class="app-box-title"><?php echo JText::_('Ergebnisse:'); ?></h2>
                    </div>
                </div>
                <div class="app-box-content">
                    <?php echo $this->item->text2; ?>
                </div>
            </div>
        <?php } ?>
        <div id="jsapp-files" class="app-box">
            <div class="app-box-header">
                <div class="app-box-header">
                    <h2 class="app-box-title"><?php echo JText::_('Beschreibung:'); ?></h2>
                </div>
            </div>
            <div class="app-box-content">
                <?php echo $this->item->text; ?>
            </div>
        </div>
    <?php else: ?>
        <?php echo JHtml::_('string.truncate', $this->item->text, $params->get('readmore_limit', 100)); ?>
        <p class="readmore">
            <?php if ($user->id == 0): ?>
                <a href="<?php echo JRoute::_('index.php?option=com_users&view=login&Itemid=' . $itemId . '&return=' . base64_encode(JRoute::_(GoodpracticeHelperRoute::getEntryRoute($this->item->id, $this->item->catid)))); ?>"><?php echo JText::_('COM_GOODPRACTICE_REGISTER_TO_READ_MORE'); ?></a>
            <?php endif; ?>
        </p>
    <?php endif; ?>
    <?php echo $this->item->event->afterDisplayContent; ?>
    <?php if ($showPrint || $showEmail) : ?>
        <ul class="actions" style="margin-top: 15px;">
            <?php if ($this->print) : ?>
                <li><?php echo JHtml::_('icon.printScreen', $this->item, $params); ?></li>
            <?php else : ?>
                <?php // if ($showPrint) :   ?>
                    <!--<li class="print-icon"><?php // echo JHtml::_('icon.printPopup', $this->item, $params);      ?></li>-->
                <?php // endif;   ?>

                <?php // if ($showEmail) :   ?>
                                                                            <!--<li class="email-icon"><?php // echo JHtml::_('icon.email', $this->item, $params);      ?></li>-->
                <?php // endif;   ?>

            <?php endif; ?>
        </ul>
    <?php endif; ?>
    <hr>
    <?php
    if ($this->item->projektart != "" || $this->item->laufzeit_von != "" || $this->item->contact_street != "" || $this->item->contact_city != "" || $this->item->laufzeit_bis != "" || $this->item->project_status != "" || $this->item->traeger != "" || $this->item->contact_web != "")
    {
        ?>
        <div id="jsapp-files" class="app-box">
            <div class="app-box-header">
                <div class="app-box-header">
                    <h2 class="app-box-title"><?php echo JText::_('Kontakt'); ?></h2>
                </div>
            </div>
            <div class="app-box-content">
                <table>
                    <?php
                    if ($this->item->projektart != "")
                    {
                        echo '<tr><td style="min-width:120px">';
                        echo JText::_('Projektart:');
                        echo '</td><td>' . $this->item->projektart;
                        echo '</td></tr>';
                    };
                    if ($this->item->laufzeit_von != "" || $this->item->laufzeit_bis != "")
                    {
                        echo '<tr><td style="min-width:120px">';
                        echo JText::_('Laufzeit:');
                        echo '</td><td>';
                        if ($this->item->laufzeit_von != "")
                            echo substr($this->item->laufzeit_von, 8, 2) . "." . substr($this->item->laufzeit_von, 5, 2) . "." . substr($this->item->laufzeit_von, 0, 4);
                        else
                            echo "?";
                        echo " - ";
                        if ($this->item->laufzeit_bis != "")
                            echo substr($this->item->laufzeit_bis, 8, 2) . "." . substr($this->item->laufzeit_bis, 5, 2) . "." . substr($this->item->laufzeit_bis, 0, 4);
                        else
                            echo "?";
                        echo '</td></tr>';
                    };
                    //if ($this->item->project_status!="") {echo '<tr><td style="min-width:120px">';echo JText::_('Status:'); echo '</td><td>'.$this->item->project_status;echo '</td></tr>';};
                    //if ($this->item->region!="") {echo '<tr><td style="min-width:120px">';echo JText::_('Region:'); echo '</td><td>'.$this->item->region;echo '</td></tr>';};
                    if ($this->item->hochschule != "")
                    {
                        echo '<tr><td style="min-width:120px">';
                        echo JText::_('Hochschule:');
                        echo '</td><td>' . $this->item->hochschule;
                        echo '</td></tr>';
                    };
                    if ($this->item->traeger != "")
                    {
                        echo '<tr><td style="min-width:120px">';
                        echo JText::_('Institution:');
                        echo '</td><td>' . $this->item->traeger;
                        echo '</td></tr>';
                    };
                    if ($this->item->contact_street != "")
                    {
                        echo '<tr><td style="min-width:120px">';
                        echo JText::_('Stra&szlig;e:');
                        echo '</td><td>' . $this->item->contact_street;
                        echo '</td></tr>';
                    };
                    if ($this->item->contact_city != "")
                    {
                        echo '<tr><td style="min-width:120px">';
                        echo JText::_('Ort:');
                        echo '</td><td>' . $this->item->contact_zip . ' ' . $this->item->contact_city;
                        echo '</td></tr>';
                    };
                    if ($this->item->contact_web != "")
                    {
                        echo '<tr><td style="min-width:120px">';
                        echo JText::_('Web:');
                        echo '</td><td><a href="' . $this->item->contact_web . '">' . $this->item->contact_web;
                        echo '</a></td></tr>';
                    };
                    ?>            
                </table></div>
        </div>
        <?php
    }

    if ($this->item->contact_name != "" || $this->item->contact_mail != "" || $this->item->contact_phone != "")
    {
        ?>

        <div id="jsapp-files" class="app-box">
            <div class="app-box-header">
                <div class="app-box-header">
                    <h2 class="app-box-title"><?php echo JText::_('Ansprechpartner'); ?></h2>
                </div>
            </div>
            <div class="app-box-content">
                <table>
                    <?php
                    if ($this->item->contact_name != "")
                    {
                        echo '<tr><td style="min-width:120px">';
                        echo JText::_('Name: ');
                        echo '</td><td>' . $this->item->contact_name;
                        echo '</td></tr>';
                    };
                    if ($this->item->contact_mail != "")
                    {
                        echo '<tr><td style="min-width:120px">';
                        echo JText::_('Email: ');
                        echo '</td><td>' . $this->item->contact_mail;
                        echo '</td></tr>';
                    };
                    if ($this->item->contact_phone != "")
                    {
                        echo '<tr><td style="min-width:120px">';
                        echo JText::_('Telefon: ');
                        echo '</td><td>' . $this->item->contact_phone;
                        echo '</td></tr>';
                    };
                    ?>                                </table></div>
        </div>
    <?php } ?>

    <?php if (isset($showComments) && $showComments): ?>
        <div id="jsapp-files" class="app-box">
            <div class="app-box-header">
                <div class="app-box-header">
                    <h2 class="app-box-title"><?php echo JText::_('Kommentare und Rezensionen'); ?></h2>
                </div>
            </div>
            <div class="app-box-content">
                <ul>
                    <?php foreach ($this->item->text2 as $key => $comment): ?>
                        <li><a href="index.php?option=com_content&view=article&id=<?= substr($comment, 0, strpos($comment, '-')) ?>" target="_blank"><?php echo substr(strstr($comment, '-'), 1); ?></a><br /></li>
                    <?php endforeach ?>
                </ul>
            </div>
        </div>
    <?php endif; ?>

    <?php if ($showPdfs): ?>
        <div id="jsapp-files" class="app-box">
            <div class="app-box-header">
                <div class="app-box-header">
                    <h2 class="app-box-title"><?php echo JText::_('COM_GOODPRACTICE_PDFS'); ?></h2>
                </div>
            </div>
            <div class="app-box-content">		
                <ul style="list-style-image: url('images/pdf_16x16.png');">
                    <?php foreach ($this->item->pdfs as $key => $pdf): ?>
                        <li><a href="pdf_show_projects.php?id=<?= $pdf->id ?>"><?= $pdf->filename ?></a><br /></li>
                    <?php endforeach ?>
                </ul>		
            </div>
        </div>
    <?php endif; ?>

    <?php if ($showLinks): ?>        
        <div id="jsapp-links" class="app-box">
            <div class="app-box-header">
                <div class="app-box-header">
                    <h2 class="app-box-title"><?php echo JText::_('COM_GOODPRACTICE_LINKS'); ?></h2>
                </div>
            </div>
            <div class="app-box-content">		
                <ul style="list-style-image: url('images/folder_link_16x16.png');">
                    <?php foreach ($this->item->links as $key => $link): ?>
                        <li><a href="<?= $link->link ?>" target="_blank"><?= $link->description ?></a><br /></li>
                    <?php endforeach ?>
                </ul>		
            </div>
        </div>
    <?php endif; ?>

    <?php if ($showVariants): ?>
        <div id="jsapp-tags" class="app-box">
            <div class="app-box-header">
                <div class="app-box-header">
                    <h2 class="app-box-title"><?php echo JText::_('Schlagworte'); ?></h2>
                </div>
            </div>
            <div class="app-box-content">		
                <ul>
                    <?php foreach ($this->item->tags as $key => $tag): ?>
                        <li><a href="<?php echo 'das-institut/Method/Method-von-a-z?searchword=' . $tag ?>" target="_blank"><?php echo $tag; ?></a><br /></li>
                    <?php endforeach ?>
                </ul>		
            </div>
        </div>

        <div id="jsapp-tags" class="app-box">
            <div class="app-box-header">
                <div class="app-box-header">
                    <h2 class="app-box-title"><?php echo JText::_('Themen'); ?></h2>
                </div>
            </div>
            <div class="app-box-content">		
                <ul>
                    <li><a href="<?php echo 'das-institut/Method/rubrikstruktur?view=category&id=' . $this->item->catid ?>" target="_blank"><?php echo $this->item->category->title; ?></a><br /></li>
                </ul>		
            </div>
        </div>


    <?php endif; ?>
</div>

