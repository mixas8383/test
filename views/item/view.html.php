<?php

/**
 * @version		$Id: view.html.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Goodpractice
 * @subpackage	Component
 * @copyright	Copyright (C) 2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla modeladmin library
jimport('joomla.application.component.view');

/**
 * Item View of Goodpractice component
 *
 * @since	0.0.1
 */
class GoodpracticeViewItem extends JViewLegacy
{

    /**
     * Execute and display a layout script.
     *
     * @param	string $tpl	The name of the layout file to parse.
     *
     * @return	void|JError
     *
     * @since	0.0.1
     *
     * @see		JView::display
     */
    public function display($tpl = null)
    {
        $app = JFactory::getApplication();
        $input = JFactory::getApplication()->input;
        // Get data from the model
        $this->item = $this->get('Item');
        $this->state = $this->get('State');
        $this->params = $this->get('Params');
        $this->print = $input->getBool('print');
        
 

        // Create a shortcut for $item.
        $item = &$this->item;

        // Check for errors.
        if (count($errors = $this->get('Errors')))
        {
            JFactory::getApplication()->enqueueMessage(implode('<br />', $errors), 'warning');
            return;
            //throw new \JViewGenericdataexception(implode('<br />', $errors), 500);
            //return JError::raiseWarning(500, implode('<br />', $errors));
        }

        // Check for not found.
        if (!$this->item || $this->item->published != 1)
        {
            throw new \JViewGenericdataexception(JText::_('COM_GOODPRACTICE_ERROR_ENTRY_NOT_FOUND'), 404);
            //return JError::raiseError(404, JText::_('COM_GOODPRACTICE_ERROR_ENTRY_NOT_FOUND'));
        }

        // Check for access
        $user = JFactory::getUser();
        $view_levels = $user->getAuthorisedViewLevels();
        if (!in_array($this->item->access, $view_levels))
        {
            if ($this->params->get('show_noauth', 0))
            {
                if ($user->id)
                {
                    JFactory::getApplication()->enqueueMessage(JText::_('JERROR_ALERTNOAUTHOR'), 'notice');
                    return;
                    //throw new \JViewGenericdataexception(JText::_('JERROR_ALERTNOAUTHOR'), 403);
                    //JError::raiseNotice(403, JText::_('JERROR_ALERTNOAUTHOR'));
                }
            } else
            {
                throw new \JViewGenericdataexception(JText::_('JERROR_ALERTNOAUTHOR'), 403);
                //  return JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
            }
        }
        $offset = $input->getInt('start');

        // Process the content plugins.
        JPluginHelper::importPlugin('content');
        $results = $app->triggerEvent('onContentPrepare', array('com_goodpractice.entry', &$item, &$this->params, $offset));

        $item->event = new stdClass();
        $results = $app->triggerEvent('onContentAfterTitle', array('com_goodpractice.entry', &$item, &$this->params, $offset));
        $item->event->afterDisplayTitle = trim(implode("\n", $results));

        $results = $app->triggerEvent('onContentBeforeDisplay', array('com_goodpractice.entry', &$item, &$this->params, $offset));
        $item->event->beforeDisplayContent = trim(implode("\n", $results));

        $results = $app->triggerEvent('onContentAfterDisplay', array('com_goodpractice.entry', &$item, &$this->params, $offset));
        $item->event->afterDisplayContent = trim(implode("\n", $results));


        // Increment the hit
        $this->getModel()->hit();

        // Prepare the document
        $this->prepareDocument();

        // Display the layout
        parent::display($tpl);
    }

    /**
     * Method to set up the document properties
     *
     * @since	0.0.1
     *
     * @return void
     */
    protected function prepareDocument()
    {
        $app = JFactory::getApplication();
        $menus = $app->getMenu();
        $document = JFactory::getDocument();
        $pathway = $app->getPathway();

        // Load specific css component
        JHtml::_('stylesheet', 'com_goodpractice/goodpractice.css', array(), true);

        // Because the application sets a default page title,
        // we need to get it from the menu item itself
        // Because the application sets a default page title,
        // we need to get it from the menu item itself
        $menu = $menus->getActive();
        $id = (int) @$menu->query['id'];

        if ($menu && ($menu->query['option'] != 'com_goodpractice' || $menu->query['view'] == 'item' || $id != $this->category->id))
        {
            $path = array(array('title' => $this->item->title, 'link' => ''));
            $category = JCategories::getInstance('Goodpractice')->get($this->item->catid);

            while ($category && ($menu->query['option'] != 'com_goodpractice' || $menu->query['view'] == 'item' || $id != $category->id) && $category->id > 1)
            {
                $path[] = array('title' => $category->title, 'link' => GoodpracticeHelperRoute::getCategoryRoute($category->id));
                $category = $category->getParent();
            }
            $path = array_reverse($path);
            foreach ($path as $item)
            {
                $pathway->addItem($item['title'], $item['link']);
            }
        }
        if ($menu)
        {
            $this->params->def('page_heading', $this->params->get('page_title', $menu->title));
        } else
        {
            $this->params->def('page_heading', JText::_('COM_GOODPRACTICE_DEFAULT_PAGE_TITLE'));
        }

        // Set the page title
        $title = $this->params->get('page_title', '');
        if (empty($title))
        {
            $title = $app->getCfg('sitename');
        } elseif ($app->getCfg('sitename_pagetitles', 0))
        {
            $title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
        }
        $document->setTitle($title);

        // Add the entry meta-description
        if ($this->item->metadesc)
        {
            $this->document->setDescription($this->item->metadesc);
        }

        // Add the entry meta-keys
        if ($this->item->metakey)
        {
            $this->document->setMetadata('keywords', $this->item->metakey);
        }

        // Add the entry meta-title
        if ($app->getCfg('MetaTitle') == '1')
        {
            $this->document->setMetaData('title', $this->item->title);
        }

        // Add the entry meta-author
        if ($app->getCfg('MetaAuthor') == '1')
        {
            $this->document->setMetaData('author', $this->item->author);
        }

        // Add all metadata
        $metadata = $this->item->metadata;
        foreach ($metadata as $k => $v)
        {
            if ($v)
            {
                $this->document->setMetadata($k, $v);
            }
        }

        // Add robots info
        if ($this->print)
        {
            $this->document->setMetaData('robots', 'noindex, nofollow');
        } else if ($this->params->get('robots'))
        {
            $this->document->setMetadata('robots', $this->params->get('robots'));
        }
    }

}
