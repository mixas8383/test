<?php
/**
 * @version		$Id: default.php 36 2011-03-30 15:24:58Z chdemko $
 * @package		Goodpractice
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Christophe Demko. All rights reserved.
 * @author		Christophe Demko
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Create shortcuts to parameters.
$params = $this->params;


JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

$showPageHeading = $params->def('show_page_heading', 1);
?>
<div class="goodpractice-lastmodified<?php echo $params->get('pageclass_sfx'); ?>">
    <?php if ($showPageHeading) : ?>
        <h1><?php echo $this->escape($params->get('page_heading')); ?></h1>
    <?php endif; ?>

    <div class="lastmodified-items">

        <!--<h3><?php echo JText::_('COM_GOODPRACTICE_CATEGORY_ENTRIES'); ?></h3>-->
        <?php echo $this->loadTemplate('items'); ?>

    </div>
</div>

