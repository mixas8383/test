<?php
/**
 * @version		$Id: default_items.php 63 2011-04-27 01:35:59Z chdemko $
 * @package		Goodpractice
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Christophe Demko. All rights reserved.
 * @author		Christophe Demko
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

//\Joomla\CMS\HTML\HTMLHelper::core();
$input = JFactory::getApplication()->input;
$params = $this->params;
$prefix = $input->getVar('prefix', $params->get('prefix', ''));
$dlang = $input->getVar('dlang', $params->get('dlang', ''));
?>

<table class="category">
    <?php if ($params->get('show_headings', 1)) : ?>
        <thead><?php echo $this->loadTemplate('head'); ?></thead>
    <?php endif; ?>
    <tbody><?php echo $this->loadTemplate('body'); ?></tbody>
    <?php if ($params->get('show_pagination', 1)) : ?>
        <tfoot><?php echo $this->loadTemplate('foot'); ?></tfoot>
    <?php endif; ?>
</table>


