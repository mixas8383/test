<?php
/**
 * @version		$Id: default_body.php 63 2011-04-27 01:35:59Z chdemko $
 * @package		Goodpractice
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Christophe Demko. All rights reserved.
 * @author		Christophe Demko
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
error_reporting(E_ALL);
// Get the view levels
$user = JFactory::getUser();
$access = $user->getAuthorisedViewLevels();
$firstletters = array();
$blankline = false;
?>
<?php foreach ($this->items as $i => $item) : ?>
    <tr class="<?php echo ($item->published == 0) ? 'system-unpublished' : ''; ?> cat-list-row<?php echo $i % 2; ?>">



        <td>
            <span style='font-weight:bold;'><a href="<?php echo JRoute::_(GoodpracticeHelperRoute::getEntryRoute($item->id)); ?>"><?php echo $item->title ?></span>
        </td>
        <td>
            <?php
            $modifiedParts = explode(' ', $item->modified);
            $dateParts = explode('-', $modifiedParts[0]);
            $date = date('d.m.Y', mktime(0, 0, 0, $dateParts[1], $dateParts[2], $dateParts[0]));
            echo $date . ' ' . $modifiedParts[1];
            ?>
        </td>
        <td align="right">
            <?php echo $item->hits; ?>
        </td>
    </tr>
<?php endforeach; ?>

