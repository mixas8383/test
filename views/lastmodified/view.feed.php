<?php

defined('_JEXEC') or die;

jimport('joomla.application.component.view');

class GoodpracticeViewLastModified extends JView // changes made for the Goodpractice!!!
{

    function display($tpl = null)
    {
        $input = JFactory::getApplication()->input;
        // parameters
        $app = JFactory::getApplication();
        $db = JFactory::getDbo();
        $document = JFactory::getDocument();
        $params = $app->getParams();
        $feedEmail = ($app->getCfg('feed_email')) ? $app->getCfg('feed_email') : 'author';
        $siteEmail = $app->getCfg('mailfrom');
        $document->link = JRoute::_('index.php?option=com_goodpractice&view=lastmodified'); // changes made for the Goodpractice!!!
        // Get some data from the model
        //JRequest::setVar('limit', $app->getCfg('feed_limit'));
        $input->setVar('limit', 20); // changes made for Goodpractice lastmodified!
        $categories = JCategories::getInstance('Goodpractice'); // changes made for the Goodpractice!!!
        $rows = $this->get('Items');
        foreach ($rows as $row)
        {
            // strip html from feed item title
            $title = $this->escape($row->title);
            $title = html_entity_decode($title, ENT_COMPAT, 'UTF-8');

            // Compute the article slug
            $row->slug = $row->alias ? ($row->id . ':' . $row->alias) : $row->id;

            // url link to article
            $link = JRoute::_(GoodpracticeHelperRoute::getEntryRoute($row->slug, $row->catid)); // changes made for the Goodpractice!!!
            // strip html from feed item description text
            // TODO: Only pull fulltext if necessary (actually, just get the necessary fields).

            $textLength = 160;
            if (isset($row->text) && strlen($row->text) > $textLength)
            {
                $description = strip_tags(substr($row->text, 0, $textLength) . ' ...');
            } else
            {
                $description = $row->text;
            }

            $author = $row->created_by_alias ? $row->created_by_alias : $row->author;

            // load individual item creator class
            $item = new JFeedItem();
            $item->title = $title;
            $item->link = $link;
            $item->description = $description;
            if ($row->modified == '0000-00-00 00:00:00')
            { // changes made for Goodpractice lastmodified!
                $item->date = $row->created;
            } else
            {
                $item->date = $row->modified; // $row->created;
            }
            $item_category = $categories->get($row->catid);
            $item->category = array();
            //$item->category[]	= JText::_('JFEATURED'); // All featured articles are categorized as "Featured"
            for ($item_category = $categories->get($row->catid); $item_category !== null; $item_category = $item_category->getParent())
            {
                if ($item_category->id > 1)
                { // Only add non-root categories
                    $item->category[] = $item_category->title;
                }
            }

            $item->author = $author;
            if ($feedEmail == 'site')
            {
                $item->authorEmail = $siteEmail;
            } else
            {
                $item->authorEmail = $row->author_email;
            }
            // loads item info into rss array
            $document->addItem($item);
        }
    }

}

?>