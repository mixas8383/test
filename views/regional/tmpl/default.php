<?php
/**
 * @version		$Id: default.php 36 2011-03-30 15:24:58Z bfoecke $
 * @package		Goodpractice
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
$input = JFactory::getApplication()->input;
// Create shortcuts to parameters.
$params = $this->params;
$region = $input->getVar('id', $params->get('id', ''));

JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

// Get the PathWay object from the application
$app = JFactory::getApplication();
$pathway = $app->getPathway();
$items = $pathway->getPathWay();

$count = count($items);

for ($i = 0; $i < $count; $i ++)
{
    $items[$i]->name = stripslashes(htmlspecialchars($items[$i]->name, ENT_COMPAT, 'UTF-8'));
    $items[$i]->link = JRoute::_($items[$i]->link);
}

$showPageHeading = $params->def('show_page_heading', 1);
?>
<!--
datatable is initialised in the template

<script type="text/javascript" language="javascript" src="components/com_goodpractice/views/regional/tmpl/jquery.dataTables.js"></script>-->
<script type="text/javascript">
    var asInitVals = new Array();
    jQuery(document).ready(function () {
        var oTable = jQuery('#goodpractice_items').dataTable({
            "bJQueryUI": true,
            "oLanguage": {
                "sLengthMenu": "Zeige _MENU_ Eintr&auml;ge pro Seite",
                "sZeroRecords": "Keine Treffer",
                "sInfo": "Zeige _START_ bis _END_ von _TOTAL_ Eintr&auml;gen",
                "sInfoEmpty": "Zeige 0 bis 0 von 0 Eintr&auml;gen",
                "sInfoFiltered": "(gefiltert von _MAX_ Gesamteintr&auml;gen)",
                "sSearch": "Suche"
            },
            "bPaginate": false,
            "bLengthChange": false,
            "bInfo": false,
            "bScrollCollapse": true,
            "aoColumnDefs": [
                {"bVisible": false, "aTargets": [2, 3, 4]},
                {"bSearchable": true, "aTargets": [0, 1, 2, 3, 4]}
            ]
        });
        jQuery("tfoot input").keyup(function () {
            /* Filter on the column (the index) of this element */
            oTable.fnFilter(this.value, jQuery("tfoot input").index(this));
        });

        oTable.fnFilter("<?= $region ?>", 4);

        /*
         * Support functions to provide a little bit of user friendlyness to the textboxes in 
         * the footer
         */
        jQuery("tfoot input").each(function (i) {
            asInitVals[i] = this.value;
        });

        jQuery("tfoot input").focus(function () {
            if (this.className == "search_init")
            {
                this.className = "";
                this.value = "";
            }
        });

        jQuery("tfoot input").blur(function (i) {
            if (this.value == "")
            {
                this.className = "search_init";
                this.value = asInitVals[jQuery("tfoot input").index(this)];
            }
        });


    });
</script>

<div class="goodpractice-regional<?php echo $params->get('pageclass_sfx'); ?>" style="margin-top:20px">
    <?php if ($showPageHeading) : ?>
        <h1><?php echo $this->escape($params->get('page_heading')); ?></h1>
    <?php endif; ?>

    <div class="regional-items">

        <!--<h3><?php echo JText::_('COM_GOODPRACTICE_CATEGORY_ENTRIES'); ?></h3>-->
        <?php echo $this->loadTemplate('items'); ?>

    </div>
</div>

