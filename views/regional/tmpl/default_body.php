<?php
/**
 * @version		$Id: default_body.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Goodpractice
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
error_reporting(E_ALL);
// Get the view levels
$user = JFactory::getUser();
$access = $user->getAuthorisedViewLevels();
$firstletters = array();
$blankline = false;
?>
<?php
foreach ($this->items as $i => $item) :
    $params = $this->params;
    ?>
    <tr class="<?php echo ($item->published == 0) ? 'system-unpublished' : ''; ?> cat-list-row<?php echo $i % 2; ?>">



        <td style="text-align:left !important;">

            <?php /*
              $first_char = $item->title{0};
              $utf8marker=chr(128);
              if($first_char>=$utf8marker) {
              $parsechar=substr($item->title,0,2);
              } else {
              $parsechar=$item->title{0};
              }
              $parsechar = strtoupper($parsechar); */
            ?>
            <?php /* if (!in_array($parsechar, $firstletters)) : ?>
              <?php $firstletters[] = $parsechar; ?>
              <?php $blankline = true; ?>
              <span style='font-weight:bold;font-size:120%;'><?php echo $parsechar; ?></span><br />
              <?php endif; */ ?>
            <span style='font-weight:bold;'><a href="<?php echo JRoute::_('index.php?option=com_goodpractice&view=item&id=' . $item->id); ?>"><?php /* if (strlen($item->title)<80) */ echo $item->title; /* else echo substr($item->title, 0, 80)."..."; */ ?></a></span>

        </td>
        <td nowrap="nowrap">
            <a href="<?php echo JRoute::_('index.php?option=com_goodpractice&view=category&id=' . $item->catid); ?>"><?php echo $this->escape($item->category_title); ?></a>
        </td>

        <td nowrap="nowrap">
    <?php echo $item->contact_traeger; ?>
        </td>
        <td>
    <?php echo $item->hochschule; ?>
        </td>
        <td nowrap="nowrap">
            <a href="<?php echo JRoute::_('index.php?option=com_goodpractice&view=regional&id=' . $item->region); ?>"><?php echo $this->escape($item->region); ?></a>
        </td>
    </tr>
<?php endforeach; ?>

