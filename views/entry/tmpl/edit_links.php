<?php
/**
 * @version		$Id: edit_params.php 56 2011-04-05 20:20:35Z chdemko $
 * @package		Goodpractice
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Christophe Demko. All rights reserved.
 * @author		Christophe Demko
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

$action_link_create = JURI::base() . 'index.php?option=com_goodpractice&task=link.add&eid=' . $this->item->id . '&format=json';
$action_link_remove = JURI::base() . 'index.php?option=com_goodpractice&task=link.remove&format=json';
?>

<div id="linktext"></div>
<fieldset class="adminform">
    <legend><?php echo JText::_('COM_GOODPRACTICE_LINK_ADD'); ?>: <img style="float:none; margin: 0; cursor: pointer;" src="../media/com_goodpractice/images/administrator/icon-16-goodpractice-entry-new.png" alt="COM_GOODPRACTICE_LINK_ADD_INPUT" title="COM_GOODPRACTICE_LINK_ADD_INPUT" onclick="javascript:Joomla.addlink(document.getElementById('link').value, document.getElementById('link_description').value, '<?= $action_link_create ?>')" /></legend>
</fieldset>
<div class="row-fluid">
    <div class="span6">      
        <div class="control-group">
            <div class="control-label"><?php echo JText::_('COM_GOODPRACTICE_LINK_TEXT'); ?></div>
            <div class="controls"><input type="text" id="link" value="http://" /></div>
        </div>
        <div class="control-group">
            <div class="control-label"><?php echo JText::_('COM_GOODPRACTICE_LINK_DESC'); ?></div>
            <div class="controls"><input type="text" id="link_description" value="" /></div>
        </div>
    </div>
</div>
<fieldset class="adminform">
    <legend><?php echo JText::_('Vorhandene Links:'); ?></legend>
</fieldset>
<div class="row-fluid">
    <div class="span6" id="linklist">
        <?php foreach ($this->item->links as $link): ?>
            <div id="link_<?= $link->id ?>"><a style="color: #078502;" href="<?= $link->link ?>" title="<?= $link->description ?>"><?= $link->description ?></a>&nbsp;<img style="float: none; margin: 0px !important; padding: 0px; cursor: pointer;" onclick="javascript:Joomla.removelink(<?= $link->id ?>, '<?= $action_link_remove ?>');" src="../media/com_goodpractice/images/administrator/icon-12-goodpractice-pdf-delete.png" alt="remove" /></div>
            <?php endforeach; ?>
    </div>
</div>
<?php echo $this->form->getLabel('linkadd'); ?> <?php echo $this->form->getInput('linkadd'); ?>
