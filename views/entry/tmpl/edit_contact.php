<?php
/**
 * @version		$Id: edit_attribs.php 56 2011-04-05 20:20:35Z bfoecke $
 * @package		Goodpractice
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
?>
<div class="row-fluid">
    <div class="span6">
        <div class="control-group">
            <?php echo $this->form->getLabel('contact_name'); ?>
            <div class="controls">
                <?php echo $this->form->getInput('contact_name'); ?>
            </div>
        </div>        
        <div class="control-group">
            <?php echo $this->form->getLabel('contact_phone'); ?>
            <div class="controls">
                <?php echo $this->form->getInput('contact_phone'); ?>
            </div>
        </div>
        <div class="control-group">
            <?php echo $this->form->getLabel('contact_mail'); ?>
            <div class="controls">
                <?php echo $this->form->getInput('contact_mail'); ?>
            </div>
        </div>
    </div>
</div>

