<?php

/**
 * @version		$Id: view.html.php 63 2011-04-27 01:35:59Z chdemko $
 * @package		Goodpractice
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Christophe Demko. All rights reserved.
 * @author		Christophe Demko
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

/**
 * Category View of Goodpractice component
 *
 * @since	0.0.1
 */
class GoodpracticeViewSitemap extends JViewLegacy
{

    /**
     * Execute and display a layout script.
     *
     * @param	string $tpl	The name of the layout file to parse.
     *
     * @return	void|JError
     *
     * @since	0.0.1
     *
     * @see		JView::display
     */
    public function display($tpl = null)
    {
        // Get some data from the models
        $this->category = $this->get('Category');
        $this->state = $this->get('State');
        $this->params = $this->get('Params');
        $this->items = $this->get('Items');


//                echo "<ul>";
//                echo("<li>".$this->category->title."</li>");
//                foreach($this->category->getChildren() as $child){
//                    echo "<ul>";
//                    echo("<li>".$child->title."</li>");
//
//                    foreach($child->getChildren() as $child1){
//                        echo "<ul>";
//                        echo("<li>".$child1->title."</li>");
//                        echo "</ul>";
//                    }
//                    echo "</ul>";
//                }
//                echo "</ul>";
//
//                exit;
//                // If only one entry and no subcategories
//                if(count($this->items) == 1 && count($this->category->getChildren()) == 0){
//                    return 'false';
//                }
        // Check for errors.
        if (count($errors = $this->get('Errors')))
        {
            JFactory::getApplication()->enqueueMessage(implode('<br />', $errors), 'warning');
            return;
            // throw new \JViewGenericdataexception(implode('<br />', $errors), 500);
            //return JError::raiseWarning(500, implode('<br />', $errors));
        }

        // Check for not found.
        if (!$this->category)
        {
            throw new \JViewGenericdataexception(JText::_('COM_GOODPRACTICE_ERROR_CAGEGORY_NOT_FOUND'), 404);
            //return JError::raiseError(404, JText::_('COM_GOODPRACTICE_ERROR_CAGEGORY_NOT_FOUND'));
        }

        // Check for access
        $user = JFactory::getUser();
        $view_levels = $user->getAuthorisedViewLevels();
        if (!in_array($this->category->access, $view_levels))
        {
            if ($this->params->get('show_noauth', 0))
            {
                if ($user->id)
                {
                    JFactory::getApplication()->enqueueMessage(JText::_('JERROR_ALERTNOAUTHOR'), 'notice');
                    return;
                    //throw new \JViewGenericdataexception(JText::_('JERROR_ALERTNOAUTHOR'), 403);
                    //JError::raiseNotice(403, JText::_('JERROR_ALERTNOAUTHOR'));
                }
            } else
            {

                throw new \JViewGenericdataexception(JText::_('JERROR_ALERTNOAUTHOR'), 403);
                //return JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
            }
        }

        // Process the content plugins.
        $this->category->description = JHtml::_('content.prepare', $this->category->description);

        // Prepare the document
        $this->prepareDocument();

        // Display the layout
        parent::display($tpl);
    }

    /**
     * Method to set up the document properties
     *
     * @since	0.0.1
     *
     * @return void
     */
    protected function prepareDocument()
    {
        $app = JFactory::getApplication();
        $document = JFactory::getDocument();


        // Load specific css component
        JHtml::_('stylesheet', 'com_goodpractice/goodpractice.css', array(), true);

        // Because the application sets a default page title,
        // we need to get it from the menu item itself

        $menu = $app->getMenu()->getActive();
        if ($menu)
        {
            $this->params->def('page_heading', $this->params->get('page_title', $menu->title));
        } else
        {
            $this->params->def('page_heading', JText::_('COM_GOODPRACTICE_DEFAULT_PAGE_TITLE'));
        }

        // Set the page title
        $title = $this->params->get('page_title', '');
        if (empty($title))
        {
            $title = $app->getCfg('sitename');
        } elseif ($app->getCfg('sitename_pagetitles', 0))
        {
            $title = JText::sprintf('JPAGETITLE', $app->getCfg('sitename'), $title);
        }
        $document->setTitle($title);

        // Add the category meta-description
        if ($this->category->metadesc)
        {
            $document->setDescription($this->category->metadesc);
        }

        // Add the category meta-keys
        if ($this->category->metakey)
        {
            $document->setMetadata('keywords', $this->category->metakey);
        }

        // Add the category meta-title
        if ($app->getCfg('MetaTitle'))
        {
            $document->setMetaData('title', $this->category->title);
        }

        // Add the category meta-author
        if ($app->getCfg('MetaAuthor'))
        {
            $document->setMetaData('author', $this->category->getAuthor()->name);
        }

        // Add all metadata
        $metadata = $this->category->getMetadata()->toArray();
        foreach ($metadata as $k => $v)
        {
            if ($v)
            {
                $document->setMetadata($k, $v);
            }
        }

        // Add robots info
        if ($this->params->get('robots'))
        {
            $this->document->setMetadata('robots', $this->params->get('robots'));
        }

        // Add alternative feed link
        if ($this->params->get('show_feed_link', 1) == 1)
        {
            $link = '&format=feed&limitstart=';
            $attribs = array('type' => 'application/rss+xml', 'title' => 'RSS 2.0');
            $document->addHeadLink(JRoute::_($link . '&type=rss'), 'alternate', 'rel', $attribs);
            $attribs = array('type' => 'application/atom+xml', 'title' => 'Atom 1.0');
            $document->addHeadLink(JRoute::_($link . '&type=atom'), 'alternate', 'rel', $attribs);
        }
    }

}
