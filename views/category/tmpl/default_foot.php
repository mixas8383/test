<?php
/**
 * @version		$Id: default_foot.php 20 2011-03-28 23:33:03Z chdemko $
 * @package		Goodpractice
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Christophe Demko. All rights reserved.
 * @author		Christophe Demko
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
?>
<tr class="pagination">
    <td colspan="10"><?php echo $this->pagination->getListFooter(); ?></td>
</tr>

