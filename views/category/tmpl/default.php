<?php
/**
 * @version		$Id: default.php 63 2011-04-27 01:35:59Z chdemko $
 * @package		Goodpractice
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Christophe Demko. All rights reserved.
 * @author		Christophe Demko
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Get access view levels
$user = JFactory::getUser();
$access = $user->getAuthorisedViewLevels();

// Get current menu item
$menu = JFactory::getApplication()->getMenu();
$active = $menu->getActive();
if ($active)
{
    $itemId = $active->id;
} else
{
    $itemId = 0;
}

// Create shortcuts to parameters.
$params = $this->params;
$children = $this->category->getChildren();
$parent = $this->category->getParent();

// Include Html helpers
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

// Compute some flags
$showPageHeading = $params->def('show_page_heading', 1);
$showCategoryTitle = $params->get('show_category_title', 1);
$showDescription = $params->get('show_description', 1) && $this->category->description;
$showImage = $params->get('show_description_image', 1) && $this->category->getParams()->get('image');
;
$showChildren = count($children);
$showParent = $params->get('show_parent_category') && $parent;
$showItems = !empty($this->items);
?>
<div id="goodpractice" class="goodpractice-category<?php echo $params->get('pageclass_sfx'); ?>">
    <?php if ($showPageHeading) : ?>
        <h1><?php echo $this->escape($params->get('page_heading')); ?></h1>
    <?php endif; ?>
    <?php if ($showParent) : ?>
        <div class="category-parent">
            <table border="1" style="width: 100%;">
                <tr>
                    <td style="width: 33%; text-align: center;">
                        <?php if ($this->leftSibling) : ?>
                            <span class="item-title">
                                <?php echo JHtml::_('icon.prev', $this->leftSibling, $params); ?>
                            </span>
                        <?php endif; ?>
                    </td>
                    <td style="width: 33%; text-align: center;">
                        <span class="item-title">
                            <a href="<?php echo JRoute::_(GoodpracticeHelperRoute::getCategoryRoute($parent->id)); ?>" title="<?php echo $parent->id == 'root' ? JText::_('COM_GOODPRACTICE_ROOT_TITLE') : $this->escape($parent->title); ?>">^</a>
                        </span>
                    </td>
                    <td style="width: 33%; text-align: center;">
                        <?php if ($this->rightSibling) : ?>
                            <span class="item-title">
                                <?php echo JHtml::_('icon.next', $this->rightSibling, $params); ?>
                            </span>
                        <?php endif; ?>
                    </td>
                </tr>
            </table>
        </div>
    <?php endif; ?>
    <?php if ($showCategoryTitle) : ?>
        <h2><?php echo JHtml::_('content.prepare', $this->category->id == 'root' ? JText::_('COM_GOODPRACTICE_ROOT_TITLE') : $this->category->title); ?></h2>
    <?php endif; ?>

    <?php if ($showDescription || $showImage) : ?>
        <?php if ($showImage): ?>
            <img src="<?php echo $this->category->getParams()->get('image'); ?>"/>
        <?php endif; ?>
        <?php if ($showDescription): ?>
            <div id="jsapp-parent" class="app-box">
                <!--<div class="app-box-header">
                        <div class="app-box-header">
                                <h2 class="app-box-title"><?php echo JText::_('COM_GOODPRACTICE_CATEGORY_DESC'); ?></h2>
                        </div>
                </div>-->
                <div class="app-box-content">		
                    <span class="item-title">
                        <?php echo $this->category->description; ?>
                    </span>
                </div>
            </div>

        <?php endif; ?>

    <?php endif; ?>

    <!-- Artikel zur Einf�hrung und zur Vertiefung z�hlen -->
    <?php
    $count = 0;
    $count2 = 0;
    foreach ($this->items as $i => $item) : if ($item->introduction == 1) : $count += 1;
        else: $count2 += 1;
        endif;
    endforeach;
    ?>

    <!-- Artikel zur Einf�hrung ausgeben, falls vorhanden -->
<?php if ($count > 0) : ?>
        <div id="jsapp-children" class="app-box">
            <div class="app-box-header">
                <div class="app-box-header">
                    <h2 class="app-box-title"><?php echo JText::_('Projekte zur Einf&uuml;hrung'); ?></h2>
                </div>
            </div>
            <div class="app-box-content">
                <ul>
                    <?php foreach ($this->items as $i => $item) : ?>

        <?php if ($item->introduction == 1) : ?>
                            <li class="<?php echo ($item->published == 0) ? 'system-unpublished' : ''; ?> cat-list-row<?php echo $i % 2; ?>">

                                <a href="<?php echo JRoute::_(GoodpracticeHelperRoute::getEntryRoute($item->slug, $item->catid)); ?>"><?php echo $item->title; ?></a>


                                                            <!--<p align="right">
            <?php echo $item->hits; ?>
                                                            </p>-->
                            </li>
                        <?php endif; ?>
    <?php endforeach; ?>
                </ul>
            </div>
        </div>

    <?php endif; ?>

<?php if ($showParent) : ?>
        <div id="jsapp-parent" class="app-box">
            <div class="app-box-header">
                <div class="app-box-header">
                    <h2 class="app-box-title"><?php echo JText::_('COM_GOODPRACTICE_PARENT_CATEGORY'); ?></h2>
                </div>
            </div>
            <div class="app-box-content">		
                <span class="item-title">
                    <a href="<?php echo JRoute::_(GoodpracticeHelperRoute::getCategoryRoute($parent->id)); ?>"><?php echo $parent->id == 'root' ? JText::_('COM_GOODPRACTICE_ROOT_TITLE') : $this->escape($parent->title); ?></a>
                </span>
            </div>
        </div>

    <?php endif; ?>

<?php if ($showChildren) : ?>
        <div id="jsapp-children" class="app-box">
            <div class="app-box-header">
                <div class="app-box-header">
                    <h2 class="app-box-title"><?php echo JText::_('Themenfelder'); ?></h2>
                </div>
            </div>
            <div class="app-box-content">		
                <ul>
                    <?php foreach ($children as $id => $child) : ?>
        <?php if ($this->params->get('show_noauth', 0) || in_array($child->access, $access)): ?>
                            <li>
                                <span class="item-title">
                                    <a href="<?php echo JRoute::_(GoodpracticeHelperRoute::getCategoryRoute($child->id)); ?>"><?php echo $this->escape($child->title); ?></a>
                                </span>
                            </li>
                        <?php endif; ?>
    <?php endforeach; ?>
                </ul>	
            </div>
        </div>
    <?php endif; ?>

<?php if ($count2 > 0) : ?>
        <div id="jsapp-children" class="app-box">
            <div class="app-box-header">
                <div class="app-box-header">
                    <h2 class="app-box-title"><?php // if ($count > 0): echo JText::_('Projekte zur Vertiefung'); else: echo JText::_('Projekte'); endif;   ?></h2>
                </div>
            </div>
            <div class="app-box-content">		
    <?php echo $this->loadTemplate('items'); ?>
            </div>
        </div>

<?php endif; ?>

</div>

