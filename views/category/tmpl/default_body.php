<?php
/**
 * @version		$Id: default_body.php 36 2011-03-30 15:24:58Z chdemko $
 * @package		Goodpractice
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Christophe Demko. All rights reserved.
 * @author		Christophe Demko
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
?>
<ul>
    <?php foreach ($this->items as $i => $item) : ?>

        <?php if ($item->introduction != 1) : ?>
            <li class="<?php echo ($item->published == 0) ? 'system-unpublished' : ''; ?> cat-list-row<?php echo $i % 2; ?>">

                <a href="<?php echo JRoute::_(GoodpracticeHelperRoute::getEntryRoute($item->slug, $item->catid)); ?>"><?php echo $item->title; ?></a>


                    <!--<p align="right">
                <?php echo $item->hits; ?>
                    </p>-->
            </li>
        <?php endif; ?>
    <?php endforeach; ?>
</ul>

