<?php

/**
 * @version		$Id: view.feed.php 63 2011-04-27 01:35:59Z chdemko $
 * @package		Goodpractice
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Christophe Demko. All rights reserved.
 * @author		Christophe Demko
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla view library
jimport('joomla.application.component.view');

// import the Joomla date library
jimport('joomla.utilities.date');

/**
 * Category View of Goodpractice component
 *
 * @since	0.0.1
 */
class GoodpracticeViewCategory extends JView
{

    /**
     * Execute and display a layout script.
     *
     * @param	string $tpl	The name of the layout file to parse.
     *
     * @return	void|JError
     *
     * @since	0.0.1
     *
     * @see		JView::display
     */
    public function display($tpl = null)
    {
        // Get the application and the document
        $app = JFactory::getApplication();
        $input = JFactory::getApplication()->input;

        // Get some data from the model
        $input->setVar('limit', $app->getCfg('feed_limit'));
        $category = $this->get('Category');
        $params = $this->get('Params');
        $rows = $this->get('Items');

        // Check for errors.
        if (count($errors = $this->get('Errors')))
        {
            JFactory::getApplication()->enqueueMessage(implode('<br />', $errors), 'error');
            return;
            //return JError::raiseWarning(500, implode('<br />', $errors));
        }

        // Check for not found.
        if (!$category)
        {
            throw new \JViewGenericdataexception(JText::_('COM_GOODPRACTICE_ERROR_CAGEGORY_NOT_FOUND'), 404);
            //return JError::raiseError(404, JText::_('COM_GOODPRACTICE_ERROR_CAGEGORY_NOT_FOUND'));
        }

        // Check for access
        $user = JFactory::getUser();
        $view_levels = $user->getAuthorisedViewLevels();
        if (!in_array($this->category->access, $view_levels))
        {
            if ($params->get('show_noauth', 0))
            {
                if ($user->id)
                {
                    JFactory::getApplication()->enqueueMessage(JText::_('JERROR_ALERTNOAUTHOR'), 'notice');
                    return;
                    //throw new \JViewGenericdataexception(JText::_('JERROR_ALERTNOAUTHOR'), 403);
                    //JError::raiseNotice(403, JText::_('JERROR_ALERTNOAUTHOR'));
                }
            } else
            {
                throw new \JViewGenericdataexception(JText::_('JERROR_ALERTNOAUTHOR'), 403);
                //return JError::raiseError(403, JText::_('JERROR_ALERTNOAUTHOR'));
            }
        }

        // Get the document
        $doc = JFactory::getDocument();
        $feedEmail = $app->getCfg('feed_email') ? $app->getCfg('feed_email') : 'author';
        $doc->link = JRoute::_(GoodpracticeHelperRoute::getCategoryRoute($category->id));
        foreach ($rows as $row)
        {
            // strip html from feed item title
            $title = $this->escape($row->title);
            $title = html_entity_decode($title, ENT_COMPAT, 'UTF-8');

            // url link to entry
            // & used instead of &amp; as this is converted by feed creator

            $link = JRoute::_(GoodpracticeHelperRoute::getEntryRoute($row->slug, $row->catid), false);

            // Get the description
            $description = JHtml::_('string.truncate', $row->text, 150);

            // Get the author
            $author = $row->created_by_alias ? $row->created_by_alias : $row->author_name;

            // Get the creation date
            if ($row->created)
            {
                $date = new JDate($row->created);
                $date = $date->toUnix();
            } else
            {
                $date = '';
            }

            // load individual item creator class
            $item = new JFeedItem();
            $item->title = $title;
            $item->link = $link;
            $item->description = $description;
            $item->date = $date;
            $item->category = $category->title;
            $item->author = $author;
            if ($feedEmail == 'site')
            {
                $item->authorEmail = $siteEmail;
            } else
            {
                $item->authorEmail = $row->author_email;
            }

            // loads item info into rss array
            $doc->addItem($item);
        }
    }

}
