<?php

/**
 * @version		$Id: category.php 63 2011-04-27 01:35:59Z chdemko $
 * @package		Goodpractice
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Christophe Demko. All rights reserved.
 * @author		Christophe Demko
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Entry Model from administrator
JLoader::register('GoodpracticeModelEntries', JPATH_ADMINISTRATOR . '/components/com_goodpractice/models/entries.php');

// import the Joomla categories library
jimport('joomla.application.categories');

/**
 * Category Model of Goodpractice component
 *
 * @since	0.0.1
 */
class GoodpracticeModelCategory extends GoodpracticeModelEntries
{

    /**
     * @var		array	The authorized ordering fields.
     *
     * @since	0.0.1
     *
     * @see		JModelList::$filter_fields
     */
    protected $filter_fields = array('title', 'hits', 'ordering');


    /**
     * @var		object	The category object.
     *
     * @since	0.0.1
     */
    protected $category;


    /**
     * @var		JRegistry	The application parameters.
     *
     * @since	0.0.1
     */
    protected $params;

    /**
     * Method to auto-populate the model state.
     *
     * @param	string	$ordering	An optional ordering field.
     * @param	string	$direction	An optional direction (asc|desc).
     *
     * @return	void
     *
     * @since	0.0.1
     *
     * @see		JModelList::populateState
     */
    protected function populateState($ordering = null, $direction = null)
    {
        parent::populateState($ordering, $direction);
        $input = JFactory::getApplication()->input;
        // Get the application
        $app = JFactory::getApplication();

        // Set the category id
        $id = $input->getInt('id', 0);
        $this->setState('filter.category_id', array($id));

        // Set the language
        if ($app->getLanguageFilter())
        {
            $this->setState('filter.language', array(JFactory::getLanguage()->getTag()));
        }

        // Set the published state
        $this->setState('filter.published', 1);
    }

    /**
     * Method to get category data for the current category
     *
     * @return	object
     *
     * @since	0.0.1
     */
    public function getCategory()
    {
        if (!isset($this->category))
        {
            $catids = $this->getState('filter.category_id', array('root'));
            $categories = JCategories::getInstance('Goodpractice', array('access' => false));
            $this->category = $categories->get($catids[0]);
        }
        return $this->category;
    }

    /**
     * Method to get a JDatabaseQuery object for retrieving the data set from a database.
     *
     * @return	object	A JDatabaseQuery object to retrieve the data set.
     *
     * @since	0.0.1
     *
     * @see		JModelList::getListQuery
     */
    protected function getListQuery()
    {
        // Get query from parent
        $query = parent::getListQuery();

        // Select slug
        $query->select('CONCAT_WS(":", a.id, a.alias) as slug');
        return $query;
    }

    /**
     * Method to get an array of data items.
     *
     * @return	mixed	An array of data items on success, false on failure.
     *
     * @since	0.0.2
     *
     * @see		JModelList::getItems
     */
    public function getItems()
    {
        // Set the view levels
        if ($this->getParams()->get('show_noauth', 0))
        {
            $this->setState('filter.access', false);
        } else
        {
            $this->setState('filter.access', JFactory::getUser()->getAuthorisedViewLevels());
        }

        return parent::getItems();
    }

    /**
     * Method to get the current application parameters
     *
     * @return	JRegistry	The application parameters
     *
     * @since	0.0.1
     */
    public function getParams()
    {
        if (!isset($this->params))
        {
            $this->params = JFactory::getApplication()->getParams();
            if (!isset($this->category))
            {
                $this->getCategory();
            }
            if ($this->category)
            {
                $this->params->merge($this->category->getParams());
            }
        }
        return $this->params;
    }

    /**
     * Get the left sibling (adjacent) categories.
     *
     * @return	mixed	An array of categories or false if an error occurs.
     * @since	1.6
     */
    function getLeftSibling()
    {
        // Get the database object
        $db = JFactory::getDbo();

        // Get the tags
        $query = $db->getQuery(true);
        $query->select('id');
        $query->from($db->quoteName('#__categories'));
        $query->where('rgt=' . (int) ($this->category->lft - 1));
        $query->where('level=' . (int) ($this->category->level));
        $db->setQuery($query);
        $lft_sibling = $db->loadResult();

        if (empty($lft_sibling))
        {
            return null;
        }


        $categories = JCategories::getInstance('Goodpractice', array('access' => false));
        return $categories->get($lft_sibling);
    }

    /**
     * Get the left sibling (adjacent) categories.
     *
     * @return	mixed	An array of categories or false if an error occurs.
     * @since	1.6
     */
    function getRightSibling()
    {
        // Get the database object
        $db = JFactory::getDbo();

        // Get the tags
        $query = $db->getQuery(true);
        $query->select('id');
        $query->from($db->quoteName('#__categories'));
        $query->where('lft=' . (int) ($this->category->rgt + 1));
        $query->where('level=' . (int) ($this->category->level));

        $db->setQuery($query);
        $rgt_sibling = $db->loadResult();

        if (empty($rgt_sibling))
        {
            return null;
        }
        $categories = JCategories::getInstance('Goodpractice', array('access' => false));
        return $categories->get($rgt_sibling);
    }

}
