<?php

/**
 * @version		$Id: user.php 21097 2011-04-07 15:38:03Z dextercowley $
 * @package		Joomla.Framework
 * @subpackage	Form
 * @copyright	Copyright (C) 2005 - 2011 Open Source Matters, Inc. All rights reserved.
 * @license		GNU General Public License version 2 or later; see LICENSE.txt
 */
defined('JPATH_BASE') or die;

jimport('joomla.form.formfield');

/**
 * Field to select a user id from a modal list.
 *
 * @package		Joomla.Framework
 * @subpackage	Form
 * @since		1.6
 */
class JFormFieldUser extends JFormField
{

    /**
     * The form field type.
     *
     * @var		string
     * @since	1.6
     */
    public $type = 'User';

    /**
     * Method to get the field input markup.
     *
     * @return	string	The field input markup.
     * @since	1.6
     */
    protected function getInput()
    {
        // Initialize variables.
        $html = array();

        // Initialize some field attributes.
        $attr = $this->element['class'] ? ' class="' . (string) $this->element['class'] . '"' : '';
        $attr .= $this->element['size'] ? ' size="' . (int) $this->element['size'] . '"' : '';

        // Load the current username if available.
        $table = JTable::getInstance('user');
        $user = JFactory::getUser();
        if ($this->value)
        {
            $table->load($this->value);
        } else
        {
            $table->load($user->id);
        }

        // Create a dummy text field with the user name.
        $html[] = '<div class="fltlft">';
        $html[] = '	<input type="text" id="' . $this->id . '_name"' .
                ' value="' . htmlspecialchars($table->name, ENT_COMPAT, 'UTF-8') . '"' .
                ' disabled="disabled"' . $attr . ' />';
        $html[] = '</div>';

        // Create the real field, hidden, that stored the user id.
        $html[] = '<input type="hidden" id="' . $this->id . '_id" name="' . $this->name . '" value="' . (int) $table->id . '" />';

        return implode("\n", $html);
    }

    /**
     * Method to get the filtering groups (null means no filtering)
     *
     * @return	array|null	array of filtering groups or null.
     * @since	1.6
     */
    protected function getGroups()
    {
        return null;
    }

    /**
      /**
     * Method to get the users to exclude from the list of users
     *
     * @return	array|null array of users to exclude or null to to not exclude them
     * @since	1.6
     */
    protected function getExcluded()
    {
        return null;
    }

}
