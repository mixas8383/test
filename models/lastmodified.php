<?php

/**
 * @version		$Id: alphabetical.php 63 2011-04-27 01:35:59Z chdemko $
 * @package		Goodpractice
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Christophe Demko. All rights reserved.
 * @author		Tilo Ziegler
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Entry Model from administrator
JLoader::register('GoodpracticeModelEntries', JPATH_ADMINISTRATOR . '/components/com_goodpractice/models/entries.php');

// import the Joomla categories library
jimport('joomla.application.categories');

/**
 * Alphabetical Model of Goodpractice component
 *
 * @since	0.0.1
 */
class GoodpracticeModelLastModified extends GoodpracticeModelEntries
{

    protected $params;

    /**
     * Method to auto-populate the model state.
     *
     * @param	string	$ordering	An optional ordering field.
     * @param	string	$direction	An optional direction (asc|desc).
     *
     * @return	void
     *
     * @since	0.0.1
     *
     * @see		JModelList::populateState
     */
    protected function populateState($ordering = null, $direction = null)
    {
        parent::populateState('modified', 'desc');
$input = JFactory::getApplication()->input;
        // Get the application
        $app = JFactory::getApplication();

        // Set the language
        $language =$input->getVar('dlang', '');
        if ($language)
        {
            $this->setState('filter.language', array($language));
        } else
        {
            $this->setState('filter.language', array(JFactory::getLanguage()->getTag()));
        }

        // Set the prefix
        $prefix = $input->getVar('prefix', '');
        if ($prefix)
        {
            $this->setState('filter.prefix', array($prefix));
        } else
        {
            $this->setState('filter.prefix', true);
        }

        // Set the published state
        $this->setState('filter.published', 1);
    }

    /**
     * Method to get a JDatabaseQuery object for retrieving the data set from a database.
     *
     * @return	object	A JDatabaseQuery object to retrieve the data set.
     *
     * @since	0.0.1
     *
     * @see		JModelList::getListQuery
     */
    protected function getListQuery()
    {
        // Get query from parent
        $query = parent::getListQuery();

        // Select slug
        $query->select('CONCAT_WS(":", a.id, a.alias) as slug');
        return $query;
    }

    /**
     * Method to get an array of data items.
     *
     * @return	mixed	An array of data items on success, false on failure.
     *
     * @since	0.0.2
     *
     * @see		JModelList::getItems
     */
    public function getItems()
    {
        // Set the view levels
        if ($this->getParams()->get('show_noauth'))
        {
            $this->setState('filter.access', false);
        } else
        {
            $this->setState('filter.access', JFactory::getUser()->getAuthorisedViewLevels());
        }
        return parent::getItems();
    }

    /**
      /**
     * Method to get the current application parameters
     *
     * @return	JRegistry	The application parameters
     *
     * @since	0.0.1
     */
    public function getParams()
    {
        if (!isset($this->params))
        {
            $this->params = JFactory::getApplication()->getParams();
        }
        return $this->params;
    }

}
