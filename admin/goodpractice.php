<?php

/**
 * @version		$Id: goodpractice.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Themensammluing
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Hofmann Büroorganisation. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
defined('_JEXEC') or die;
JHtml::_('behavior.tabstate');
ini_set('display_errors', 1);
ini_set('display_startup_errors', 1);


error_reporting(E_ALL & ~E_DEPRECATED & ~E_NOTICE);

if (!JFactory::getUser()->authorise('core.manage', 'com_goodpractice'))
{
    //throw new \JViewGenericdataexception( JText::_('JERROR_ALERTNOAUTHOR'), 404);
    JFactory::getApplication()->enqueueMessage(JText::_('JERROR_ALERTNOAUTHOR'), 'warning');
    return;
    //return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

JLoader::register('GoodpracticeHelper', __DIR__ . '/helpers/goodpractice.php');

$controller = JControllerLegacy::getInstance('Goodpractice');
$controller->execute(JFactory::getApplication()->input->get('task'));
$controller->redirect();

/* No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Access check.
if (!JFactory::getUser()->authorise('core.manage', 'com_goodpractice')) 
{
	return JError::raiseWarning(404, JText::_('JERROR_ALERTNOAUTHOR'));
}

// require helpers file
require_once dirname(__FILE__) . '/helpers.php';

// import joomla controller library
jimport('joomla.application.component.controller');

// Get an instance of the controller prefixed by Goodpractice
$controller = JControllerLegacy::getInstance('Goodpractice');

// Perform the Request task
$controller->execute(JRequest::getCmd('task'));

// Redirect if set by the controller
$controller->redirect();*/
