<?php

/**
 * @version		$Id: entry.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Goodpractice
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke, Inc. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
use Joomla\Utilities\ArrayHelper;
use Joomla\CMS\MVC\Controller\FormController;

// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import Joomla controllerform library
jimport('joomla.application.component.controllerform');

/**
 * Entry Controller of Goodpractice component
 *
 * @since	0.0.1
 */
class GoodpracticeControllerEntry extends FormController
{

    /**
     * Class constructor.
     *
     * @param   array  $config  A named array of configuration variables.
     *
     * @since   1.6
     */
    public function __construct($config = array())
    {
        parent::__construct($config);
    }

    /**
     * Method to check if you can add a new record.
     *
     * @param	array	An array of input data.
     *
     * @return	boolean
     *
     * @since	0.0.2
     *
     * @see		JControllerForm::allowAdd
     */
    protected function allowAdd($data = array())
    {
        // Initialise variables.
        $input = JFactory::getApplication()->input;
        $user = JFactory::getUser();
        $categoryId = ArrayHelper::getValue($data, 'catid', $input->getInt('filter_category_id'), 'int');
        if ($categoryId)
        {
            // If the category has been passed in the data or URL check it.
            return $user->authorise('core.create', 'com_goodpractice.category.' . $categoryId);
        } else
        {
            // In the absense of better information, revert to the component permissions.
            return parent::allowAdd($data);
        }
    }

    /**
     * Method to check if you can edit a record.
     *
     * @param	array	An array of input data.
     * @param	string	The name of the key for the primary key.
     *
     * @return	boolean
     *
     * @since	0.0.2
     *
     * @see		JControllerForm::allowEdit
     */
    protected function allowEdit($data = array(), $key = 'id')
    {
        // Initialise variables.
        $recordId = (int) isset($data[$key]) ? $data[$key] : 0;
        $user = JFactory::getUser();
        $userId = $user->get('id');

        // Check general edit permission first.
        if ($user->authorise('core.edit', 'com_goodpractice.entry.' . $recordId))
        {
            return true;
        }

        // Fallback on edit.own. First test if the permission is available.
        if ($user->authorise('core.edit.own', 'com_goodpractice.entry.' . $recordId))
        {
            // Now test the owner is the user.
            $ownerId = (int) isset($data['created_by']) ? $data['created_by'] : 0;
            if (empty($ownerId) && $recordId)
            {
                // Need to do a lookup from the model.
                $record = $this->getModel()->getItem($recordId);
                if (empty($record))
                {
                    return false;
                }
                $ownerId = $record->created_by;
            }

            // If the owner matches 'me' then do the test.
            if ($ownerId == $userId)
            {
                return true;
            }
        }

        // Since there is no asset tracking, revert to the component permissions.
        return parent::allowEdit($data, $key);
    }

    /**
     * Method to run batch operations.
     *
     * @param   object  $model  The model.
     *
     * @return  boolean   True if successful, false otherwise and internal error is set.
     *
     * @since   1.6
     */
    public function batch($model = null)
    {
        JSession::checkToken() or jexit(JText::_('JINVALID_TOKEN'));

        // Set the model
        $model = $this->getModel('Entry', '', array());

        // Preset the redirect
        $this->setRedirect(JRoute::_('index.php?option=com_goodpractice&view=entries' . $this->getRedirectToListAppend(), false));

        return parent::batch($model);
    }

    /**
     * Function that allows child controller access to model data after the data has been saved.
     *
     * @param   JModelLegacy  $model      The data model object.
     * @param   array         $validData  The validated data.
     *
     * @return	void
     *
     * @since	3.1
     */
    protected function postSaveHook(JModelLegacy $model, $validData = array())
    {

        return;
    }

}
