<?php

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of json
 *
 * @author tiloziegler
 */
jimport('joomla.application.component.controllerform');

use Joomla\CMS\MVC\Controller\FormController;

class GoodpracticeControllerLink extends FormController
{

    function add()
    {
        $input = JFactory::getApplication()->input;
        $timestamp = time();
        $json = $input->getVar('json');
        $data = json_decode($json);

        if (empty($data->description))
        {
            $response = array(
                'status' => 0,
                'msg' => "Bitte geben Sie eine Beschreibung ein"
            );
            echo json_encode($response);
            exit;
        }


        if (!filter_var($data->link, FILTER_VALIDATE_URL))
        {
            $response = array(
                'status' => 0,
                'msg' => "Link entspricht nicht den Vorgaben. Bitte geben Sie Ihren Link in der Form 'http://www.google.de' ein"
            );
            echo json_encode($response);
            exit;
        }

        $response = array(
            'status' => 1,
            'msg' => "Link hinzugef�gt. Der Beitrag muss gespeichert werden, damit die markierten Links gespeichert werden.",
            'entry' => '<li><a href="' . $data->link . '" style="color: #FF2929;">' . $data->description . '</a>&nbsp;<img style="float: none; margin: 0px !important; padding: 0px; cursor: pointer;" onclick="this.parentNode.parentNode.removeChild(this.parentNode);" src="../media/com_goodpractice/images/administrator/icon-12-goodpractice-pdf-delete.png" alt="remove" /><input type="hidden" value="' . $data->link . '" name="jform[link][' . $timestamp . '][link]" /><input type="hidden" value="' . $data->description . '" name="jform[link][' . $timestamp . '][description]" /></li>'
        );
        echo json_encode($response);
        exit;
    }

    function remove()
    {
        $input = JFactory::getApplication()->input;
        $json =$input->getVar('json');

        $data = json_decode($json);

        if (!filter_var($data->id, FILTER_VALIDATE_INT))
        {
            $response = array(
                'status' => 0,
                'msg' => 'Keine g�ltige ID �bergeben.'
            );
            echo json_encode($response);
            exit;
        }

        $dbo = JFactory::getDbo();                                  // JQuery Datenbank-Objekt abrufen
        $dbo->setQuery("DELETE FROM #__goodpractice_entry_links WHERE id = '{$data->id}'");
        if ($dbo->execute())
        {
            $response = array(
                'status' => 1,
                'msg' => 'Link erfolgreich gel�scht'
            );
            echo json_encode($response);
            exit;
        } else
        {
            $response = array(
                'status' => 0,
                'msg' => 'Link konnte nicht gel�scht werden'
            );
            echo json_encode($response);
            exit;
        }
    }

}
