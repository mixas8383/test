<?php
/**
 * @version		$Id: edit_content.php 56 2011-04-05 20:20:35Z bfoecke $
 * @package		Goodpractice
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');
/* $doc =& JFactory::getDocument();
  $doc->addScript(JURI::root()  .  "media/com_goodpractice/js/chosen/chosen.jquery.min.js");
  $doc->addStyleSheet(JURI::root()  .  "media/com_goodpractice/js/chosen/chosen.css");
 */
?>
<fieldset class="adminform">
    <div class="control-group form-inline"><?php echo $this->form->getLabel('title'); ?> <?php echo $this->form->getInput('title'); ?>
        <?php echo $this->form->getLabel('subtitle'); ?> <?php echo $this->form->getInput('subtitle'); ?>
    </div>
    <?php echo '<p><b>Beschreibung:</b></p>' . $this->form->getInput('text'); ?>
</fieldset>
<div class="row-fluid">
    <div class="span6">
        <!--<div class="control-group">
        <?php echo $this->form->getLabel('tags'); ?>
            <div class="controls">
        <?php echo $this->form->getInput('tags'); ?>
            </div>
        </div>        -->
        <div class="control-group">
            <?php echo $this->form->getLabel('asset_id'); ?>
            <div class="controls">
                <?php echo $this->form->getInput('asset_id'); ?>
            </div>
        </div>
    </div>
</div>
