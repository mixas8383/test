<?php
/**
 * @version		$Id: edit.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Goodpractice
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// Include the component HTML helpers.
JHtml::addIncludePath(JPATH_COMPONENT . '/helpers/html');

$j = new Joomla\CMS\Version();
$j4 = substr($j->getShortVersion(), 0, 2) === '4.' ? true : false;


JHtml::_('behavior.formvalidation');
JHtml::_('behavior.keepalive');
JHtml::_('formbehavior.chosen', 'select');

$document = JFactory::getDocument();
$document->addScript(JURI::root() . "media/com_goodpractice/js/link.js");
$document->addScript(JURI::root() . "media/com_goodpractice/js/pdf.js");

// Create shortcut to parameters.
$params = $this->state->get('params');
//$params = $params->toArray();
$user = JFactory::getUser();
$app = JFactory::getApplication();
$input = $app->input;

$assoc = isset($app->item_associations) ? $app->item_associations : 0;
$params = json_decode($params);
// This checks if the config options have ever been saved. If they haven't they will fall back to the original settings.
$editoroptions = isset($params->show_publishing_options);



if (!$editoroptions)
{
    $params->show_publishing_options = '1';
    $params->show_article_options = '1';
    $params->show_urls_images_backend = '0';
    $params->show_urls_images_frontend = '0';
}

// Check if the article uses configuration settings besides global. If so, use them.
if (!empty($this->item->attribs['show_publishing_options']))
{
    $params['show_publishing_options'] = $this->item->attribs['show_publishing_options'];
}

if (!empty($this->item->attribs['show_article_options']))
{
    $params['show_article_options'] = $this->item->attribs['show_article_options'];
}

if (!empty($this->item->attribs['show_urls_images_backend']))
{
    $params['show_urls_images_backend'] = $this->item->attribs['show_urls_images_backend'];
}
?>

<script type="text/javascript">
    Joomla.submitbutton = function (task)
    {

        if (task == 'entry.cancel' || document.formvalidator.isValid(document.getElementById('entry-form'))) {
<?php echo $this->form->getField('text')->save(); ?>
            Joomla.submitform(task, document.getElementById('entry-form'));
        } else {
            alert(Joomla.JText._('JGLOBAL_VALIDATION_FORM_FAILED', 'Some values are unacceptable'));
        }
    }
</script>

<form action="<?php echo JRoute::_('index.php?option=com_goodpractice&layout=edit&id=' . (int) $this->item->id); ?>" method="post" name="adminForm" id="entry-form" class="form-validate" enctype="multipart/form-data">

    <?php echo JLayoutHelper::render('joomla.edit.item_title', $this); ?>

    <div class="<?php echo $j4 ? 'row' : 'row-fluid' ?>">
        <!-- Begin Content -->
        <div class="<?php echo $j4 ? 'col-md-' : 'span' ?>10 form-horizontal">
            <?php echo JHtml::_('bootstrap.startTabSet', 'myTab', array('active' => 'general')); ?>

            <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'general', JText::_('Method Daten', true)); ?>
            <div class="<?php echo $j4 ? 'row' : 'row-fluid' ?> ">
                <div class="<?php echo $j4 ? 'col-md-' : 'span' ?>9">
                    <fieldset class="adminform">
                        <?php echo $this->loadTemplate('content'); ?>
                    </fieldset>
                </div>
                <div class="<?php echo $j4 ? 'col-md-' : 'span' ?>3">
                    <?php echo JLayoutHelper::render('joomla.edit.global', $this); ?>
                </div>
            </div>

            <?php echo JHtml::_('bootstrap.endTab'); ?>

            <?php // Do not show the publishing options if the edit form is configured not to.    ?>
            <?php if ($params->show_publishing_options || ( $params->show_publishing_options = '' && !empty($editoroptions))) : ?>
                <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'publishing', JText::_('Ver&ouml;ffentlichungsoptionen', true)); ?>
                <div class="<?php echo $j4 ? 'row' : 'row-fluid' ?>">
                    <div class="<?php echo $j4 ? 'col-md-' : 'span' ?>6">
                        <div class="control-group">
                            <?php echo $this->form->getLabel('alias'); ?>
                            <div class="controls">
                                <?php echo $this->form->getInput('alias'); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <div class="control-label">
                                <?php echo $this->form->getLabel('id'); ?>
                            </div>
                            <div class="controls">
                                <?php echo $this->form->getInput('id'); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <?php echo $this->form->getLabel('created_by'); ?>
                            <div class="controls">
                                <?php echo $this->form->getInput('created_by'); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <?php echo $this->form->getLabel('created_by_alias'); ?>
                            <div class="controls">
                                <?php echo $this->form->getInput('created_by_alias'); ?>
                            </div>
                        </div>
                        <div class="control-group">
                            <?php echo $this->form->getLabel('created'); ?>
                            <div class="controls">
                                <?php echo $this->form->getInput('created'); ?>
                            </div>
                        </div>
                      
                    </div>
                    <div class="<?php echo $j4 ? 'col-md-' : 'span' ?>6">
                        <?php /* <div class="control-group">
                          <?php echo $this->form->getLabel('publish_up'); ?>
                          <div class="controls">
                          <?php echo $this->form->getInput('publish_up'); ?>
                          </div>
                          </div>
                          <div class="control-group">
                          <?php echo $this->form->getLabel('publish_down'); ?>
                          <div class="controls">
                          <?php echo $this->form->getInput('publish_down'); ?>
                          </div>
                          </div> */ ?>
                        <?php if ($this->item->modified_by) : ?>
                            <div class="control-group">
                                <?php echo $this->form->getLabel('modified_by'); ?>
                                <div class="controls">
                                    <?php echo $this->form->getInput('modified_by'); ?>
                                </div>
                            </div>
                            <div class="control-group">
                                <?php echo $this->form->getLabel('modified'); ?>
                                <div class="controls">
                                    <?php echo $this->form->getInput('modified'); ?>
                                </div>
                            </div>
                        <?php endif; ?>
                    </div>
                </div>
                <?php echo JHtml::_('bootstrap.endTab'); ?>
            <?php endif; ?>


            <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'projectdetails', JText::_('Kontakt', true)); ?>
            <div class="<?php echo $j4 ? 'row' : 'row-fluid' ?>">
                <div class="<?php echo $j4 ? 'col-md-' : 'span' ?>6">
                    <!--                                            <div class="control-group">
                    <?php // echo $this->form->getLabel('projektart');   ?>
                                                                    <div class="controls">
                    <?php // echo $this->form->getInput('projektart');    ?>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                    <?php // echo $this->form->getLabel('laufzeit_von');    ?>
                                                                    <div class="controls">
                    <?php // echo $this->form->getInput('laufzeit_von');    ?>
                                                                    </div>
                                                                </div>
                                                                <div class="control-group">
                    <?php // echo $this->form->getLabel('laufzeit_bis');    ?>
                                                                    <div class="controls">
                    <?php // echo $this->form->getInput('laufzeit_bis');    ?>
                                                                    </div>
                                                                </div>-->
                    <!--                                            <div class="control-group">
                    <?php // echo $this->form->getLabel('project_status');    ?>
                                                                    <div class="controls">
                    <?php // echo $this->form->getInput('project_status');    ?>
                                                                    </div>
                                                                </div>-->
                    <!--                                            <div class="control-group">
                    <?php // echo $this->form->getLabel('region');    ?>
                                                                    <div class="controls">
                    <?php // echo $this->form->getInput('region');    ?>
                                                                    </div>
                                                                </div>-->
                    <!--                                            <div class="control-group">
                    <?php // echo $this->form->getLabel('hochschule');    ?>
                                                                    <div class="controls">
                    <?php // echo $this->form->getInput('hochschule');    ?>
                                                                    </div>
                                                                </div>-->
                    <div class="control-group">
                        <?php echo $this->form->getLabel('traeger'); ?>
                        <div class="controls">
                            <?php echo $this->form->getInput('traeger'); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo $this->form->getLabel('contact_street'); ?>
                        <div class="controls">
                            <?php echo $this->form->getInput('contact_street'); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo $this->form->getLabel('contact_zip'); ?>
                        <div class="controls">
                            <?php echo $this->form->getInput('contact_zip'); ?>
                        </div>
                    </div>
                    <div class="control-group">
                        <?php echo $this->form->getLabel('contact_city'); ?>
                        <div class="controls">
                            <?php echo $this->form->getInput('contact_city'); ?>
                        </div>
                    </div>

                    <div class="control-group">
                        <?php echo $this->form->getLabel('contact_web'); ?>
                        <div class="controls">
                            <?php echo $this->form->getInput('contact_web'); ?>
                        </div>
                    </div>
                </div>
            </div>
            <?php echo JHtml::_('bootstrap.endTab'); ?>

            <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'contact', JText::_('Ansprechpartner', true)); ?>
            <?php echo $this->loadTemplate('contact'); ?>
            <?php echo JHtml::_('bootstrap.endTab'); ?>

            <?php // echo JHtml::_('bootstrap.addTab', 'myTab', 'link-details', JText::_('Links', true));  ?>
            <?php // echo $this->loadTemplate('links'); ?>
            <?php // echo JHtml::_('bootstrap.endTab');  ?>

            <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'upload-details', JText::_('Dokumente', true)); ?>
            <?php echo $this->loadTemplate('pdfs'); ?>
            <?php echo JHtml::_('bootstrap.endTab'); ?>

            <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'metadata', JText::_('JGLOBAL_FIELDSET_METADATA_OPTIONS', true)); ?>
            <?php echo $this->loadTemplate('metadata'); ?>
            <?php echo JHtml::_('bootstrap.endTab'); ?>

            <?php if ($assoc) : ?>
                <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'associations', JText::_('JGLOBAL_FIELDSET_ASSOCIATIONS', true)); ?>
                <?php echo $this->loadTemplate('associations'); ?>
                <?php echo JHtml::_('bootstrap.endTab'); ?>
            <?php endif; ?>

            <?php if ($this->canDo->get('core.admin')) : ?>
                <?php echo JHtml::_('bootstrap.addTab', 'myTab', 'permissions', JText::_('Beitragsberechtigungen', true)); ?>
                <fieldset>
                    <?php echo $this->form->getInput('rules'); ?>
                </fieldset>
                <?php echo JHtml::_('bootstrap.endTab'); ?>
            <?php endif; ?>

            <?php echo JHtml::_('bootstrap.endTabSet'); ?>

            <input type="hidden" name="task" value="" />
            <input type="hidden" name="return" value="<?php echo $input->getCmd('return'); ?>" />
            <?php echo JHtml::_('form.token'); ?>
        </div>

        <!-- End Content -->
        <!-- Begin Sidebar -->
        <?php //echo JLayoutHelper::render('joomla.edit.details', $this);    ?>
        <!-- End Sidebar -->
    </div>
</form>
