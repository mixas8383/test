<?php

/**
 * PhoogleMap | a PHP library for Google Map API.
 * Copyright (C) 2009 Alexandre JULIEN
 *
 * This library is under MIT License
 * Everybody can contribute, redistribute, modify this content.
 *
 * This library contains resources from the Phoogle PHP library which
 * is under GNU license.
 *
 * It's an enhancement of the original PhoogleMap which more functions.
 *
 * @classDescription PhoogleMap
 * @author Alexandre JULIEN
 * @tutorial <http://www.alexandre-julien.com>
 * @package PhoogleMap
 * @license GNU / GPL / MIT
 * @version 2.1 works on PHP 5.2.x and PHP 5.3
 * @copyright 2009 Alexandre JULIEN
 */
class PhoogleMap
{

    /**
     * validPoints : array
     * Holds addresses and HTML Messages for points that are valid (ie: have longitude and latitutde)
     */
    var $validPoints = array();


    /**
     * invalidPoints : array
     * Holds addresses and HTML Messages for points that are invalid (ie: don't have longitude and latitutde)
     */
    var $invalidPoints = array();


    /**
     * mapWidth
     * width of the Google Map, in pixels
     */
    var $mapWidth = 300;


    /**
     * mapHeight
     * height of the Google Map, in pixels
     */
    var $mapHeight = 300;


    /**
     * apiKey
     * Google API Key
     */
    var $apiKey = "";


    /**
     * showControl
     * True/False whether to show map controls or not
     */
    var $showControl = true;


    /**
     * showType
     * True/False whether to show map type controls or not
     */
    var $showType = true;


    /**
     * controlType
     * string: can be 'small' or 'large'
     * display's either the large or the small controls on the map, small by default
     */
    var $controlType = array();


    /**
     * zoomLevel
     * int: 0-17
     * set's the initial zoom level of the map
     */
    var $zoomLevel = 5;


    /**
     * sideBarEnabled
     * boolean true or false
     * enable the sidebar (false by default)
     */
    var $sideBarEnabled = false;


    /**
     * defaultIcon
     * (String) Javascript definition of the default icon, standard Google Map marker icon
     * by default
     */
    var $defaultIcon = "
	var gIcon = new GIcon();
	gIcon.image = \"http://labs.google.com/ridefinder/images/mm_20_red.png\";
	gIcon.shadow = \"http://labs.google.com/ridefinder/images/mm_20_shadow.png\";
	gIcon.iconSize = new GSize(12, 20);
	gIcon.shadowSize = new GSize(22, 20);
	gIcon.iconAnchor = new GLatLng(20, 6);
	gIcon.infoWindowAnchor = new GLatLng(1, 5);";


    /**
     * @var actionClick
     */
    var $actionClick = null;


    /**
     * @var $actionOver
     */
    var $actionOver = null;


    /**
     * $marker : Marker identifier
     * @var unknown_type
     */
    var $marker = null;


    /**
     * $dragndrop : Enable or disable drag & drop
     * @var unknown_type
     */
    var $dragndrop = false;


    /**
     * $openWindowInformation : Enable or disable WindowInformation on markers
     */
    var $openWindowInformation = false;

    /**
     * Instanciation of a PhoogleMap object
     * @param $key Google Map Api Key
     * @return unknown_type
     */
    function __contruct($key)
    {
        $this->apiKey = $key;
    }

    /**
     * return sidebarstatus
     */
    function isSideBarEnabled()
    {
        return $this->sideBarEnabled;
    }

    /**
     * @function setDefaultIcon
     * @description Definition of a default icon
     */
    function setDefaultIcon($definition)
    {
        $this->defaultIcon = $definition;
    }

    /**
     * @function setShowType
     * @description Enable or disable with a flag the control view
     * @param $flag
     * @return unknown_type
     */
    function setShowType($flag)
    {
        $this->showType = $flag;
    }

    /**
     * @function getShowType
     * @desc Return the flag for control view
     * @param $flag
     * @return unknown_type
     */
    function getShowType($flag)
    {
        return $this->showType;
    }

    /**
     * @function setZoomLevel
     * @param $level
     * @desc Change the zoom level
     * @return unknown_type
     */
    public function setZoomLevel($level)
    {
        $this->zoomLevel = $level;
    }

    /**
     * @function getZoomLevel
     * @desc Return the zoom level
     * @return unknown_type
     */
    public function getZoomLevel()
    {
        return $this->zoomLevel;
    }

    /**
     * @function addControlType
     * @description add the control type "small" or "large"
     * @param $controlType
     * @return unknown_type
     */
    public function addControlType($controlType)
    {
        $this->controlType[] = $controlType;
    }

    /**
     * @function getControlType
     * @description get the control type "small" or "large"
     * @return $controlType
     */
    public function getType()
    {
        return $this->controlType;
    }

    /**  Intelligent Zoom and resizing
     * @desc Reposition algorithm for rezoom and resizing fully automatized with an intelligence
     * @return unknown_type
     */
    public function enableAutoZoom()
    {
        if (count($this->validPoints) <= 1)
        {
            return false;
        }
        $latitude_array = array();
        $latitude_array = array();
        foreach ($this->validPoints as $key => $value)
        {
            $latitude_array[] = $value['lat'];
            $longitude_array[] = $value['long'];
        }
        $minimal_latitude = min($latitude_array);
        $maximal_latitude = max($latitude_array);
        $minimal_longitude = min($longitude_array);
        $maximal_longitude = max($longitude_array);

        $central_latitude = $minimal_latitude + ($maximal_latitude - $minimal_latitude) / 2;
        $central_longitude = $minimal_longitude + ($maximal_longitude - $minimal_longitude) / 2;



        $miles = (3958.75 * acos(sin($minimal_latitude / 57.2958) * sin($maximal_latitude / 57.2958) + cos($minimal_latitude / 57.2958) * cos($maximal_latitude / 57.2958) * cos($maximal_longitude / 57.2958 - $minimal_longitude / 57.2958)));

        switch ($miles)
        {
            case ($miles < 0.2):
                $zoom = 2;
                break;
            case ($miles < 0.5):
                $zoom = 3;
                break;
            case ($miles < 1):
                $zoom = 3;
                break;
            case ($miles < 2):
                $zoom = 4;
                break;
            case ($miles < 3):
                $zoom = 5;
                break;
            case ($miles < 7):
                $zoom = 6;
                break;
            case ($miles < 15):
                $zoom = 8;
                break;

            case ($miles < 50):
                $zoom = 9;
                break;

            case ($miles < 300):
                $zoom = 12;
                break;

            case ($miles < 600):
                $zoom = 14;
                break;
            default:
                $zoom = 2;
                break;
        }

        $this->centerMap($central_latitude, $central_longitude);
        $this->zoomLevel = $zoom;
    }

    /**
     * @function getDefaultIcon
     * @return String Default Icon Definition
     */
    function getDefaultIcon()
    {
        return $this->defaultIcon;
    }

    /**
     * @function setClickFunction
     * @desc Define the click event on a marker or on sidebar
     * @param Array:parameters
     * @return void
     */
    function setActionClick($function)
    {
        $this->actionClick = $function;
    }

    /**
     * @function getActionClick
     * @desc return the click event
     * @return array: $this->actionClick
     */
    function getActionClick()
    {
        return $this->actionClick;
    }

    /**
     * @function setActionOver
     * @desc Definition of an action when the mouse is over the marker
     * @return void
     */
    function setActionOver($actionHover)
    {
        $this->actionOver = $actionHover;
    }

    /**
     * @function getActionHover
     * @desc Return an action when the mouse is over the marker
     * @return String
     */
    function getActionHover()
    {
        return $this->actionOver;
    }

    /**
     * @function enableDragnDrop
     * @desc Enable drag & drop on marker
     * @return void
     */
    function enableDragnDrop()
    {
        $this->dragndrop = true;
    }

    /**
     * Enable OpenWindowInformation
     * @return unknown_type
     */
    function enableOpenWindowInformation()
    {
        $this->openWindowInformation = true;
    }

    /**
     * @function defineDragnDropAction
     * @desc Define an action when somebody makes a drag & drog on a marker
     * @return String
     */
    function defineDragnDropAction($parameters)
    {
        $command = '';
        if ($this->sideBarEnabled == true && $parameters['action'] == 'updateSideBar')
        {
            $command = '';
        }
    }

    /**
     * @function addGeoPoint
     * @description Add's an address to be displayed on the Google Map using latitude/longitude
     * early version of this function, considered experimental
     */
    function addGeoPoint($long, $lat, $address, $title, $infoHTML, $icon = null)
    {
        $pointer = count($this->validPoints);
        $this->validPoints[$pointer]['long'] = $long;
        $this->validPoints[$pointer]['lat'] = $lat;
        $this->validPoints[$pointer]['passedAddress'] = $address;
        $this->validPoints[$pointer]['title'] = $title;
        $this->validPoints[$pointer]['htmlMessage'] = $infoHTML;
        $this->validPoints[$pointer]['icon'] = $icon;
    }

    /**
     * @function centerMap
     * @description center's Google Map on a specific point
     * (thus eliminating the need for two different show methods from version 1.0)
     */
    function centerMap($lat, $long)
    {
        $this->centerMap = "map.setCenter(new GLatLng(" . $lat . "," . $long . "), " . $this->zoomLevel . ");\n";
    }

    /**
     * @function addAddress
     * @param $address:(string) Full and true address of the marker
     * @param $htmlMessage:(String) the HTML Code for Description of the Information Window
     * @param $iconURL:(String) The URL of the ICON
     * @returns Boolean Return false if the geocode is valid or true even
     * @description Add's an address to be displayed on the Google Map
     * (thus eliminating the need for two different show methods from version 1.0)
     */
    function addAddress($address, $title = null, $htmlMessage = null, $icon = null)
    {
        if (!is_string($address))
        {
            die("All Addresses must be passed as a string");
        }
        $apiURL = "http://maps.google.com/maps/geo?&output=xml&oe=utf-8&key=" . $this->apiKey . "&q=";
        $address = $apiURL . urlencode($address);
        $addressData = file_get_contents($address);
        $results = $this->xml2array($addressData);
        $xml = simplexml_load_file($address);

        if ($results['kml']['Response']['Status']['code'] === '602' || $results['kml']['Response']['Status']['code'] === '620')
        {
            return false;
        }
        if (empty($results['kml']['Response']['Placemark']['Point']['coordinates']))
        {
            $pointer = count($this->invalidPoints);

            $this->invalidPoints[$pointer]['lat'] = $results['kml']['Response']['Placemark']['Point']['coordinates'][0];
            $this->invalidPoints[$pointer]['long'] = $results['kml']['Response']['Placemark']['Point']['coordinates'][1];
            $this->invalidPoints[$pointer]['passedAddress'] = $address;
            $this->invalidPoints[$pointer]['title'] = $title;
            $this->invalidPoints[$pointer]['htmlMessage'] = $htmlMessage;
            $this->invalidPoints[$pointer]['icon'] = $icon;
        } else
        {
            $pointer = count($this->validPoints);
            $this->validPoints[$pointer]['lat'] = $results['kml']['Response']['Placemark']['Point']['coordinates'];
            $this->validPoints[$pointer]['long'] = $results['kml']['Response']['Placemark']['Point']['coordinates'];
            $this->validPoints[$pointer]['passedAddress'] = $address;
            $this->validPoints[$pointer]['title'] = $title;
            $this->validPoints[$pointer]['htmlMessage'] = $htmlMessage;
            $this->validPoints[$pointer]['icon'] = $icon;
            $this->validPoints[$pointer]['accuracy'] = null;
            $this->validPoints[$pointer]['address'] = trim($xml->Response->Placemark->address);
            $lat = explode(",", $this->validPoints[$pointer]['lat']);
            $long = explode(",", $this->validPoints[$pointer]['lat']);
            $this->validPoints[$pointer]['lat'] = $lat[1];
            $this->validPoints[$pointer]['long'] = $long[0];
            return array('lat' => $lat[1], 'long' => $long[0], 'accuracy' => $this->validPoints[$pointer]['accuracy'], 'address' => $this->validPoints[$pointer]['address']);
        }
    }

    /**
     * @function showValidPoints
     * @param $displayType:string
     * @param $css_id:string
     * @returns nothing
     * @description Displays either a table or a list of the address points that are valid.
     * Mainly used for debugging but could be useful for showing a list of addresses
     * on the map
     */
    function showValidPoints($displayType, $css_id)
    {
        $total = count($this->validPoints);
        if ($displayType == "table")
        {
            echo "<table id=\"" . $css_id . "\">\n<tr>\n\t<td>Address</td>\n</tr>\n";
            for ($t = 0; $t < $total; $t++)
            {
                echo"<tr>\n\t<td>" . $this->validPoints[$t]['passedAddress'] . "</td>\n</tr>\n";
            }
            echo "</table>\n";
        }
        if ($displayType == "list")
        {
            echo "<ul id=\"" . $css_id . "\">\n";
            for ($lst = 0; $lst < $total; $lst++)
            {
                echo "<li>" . $this->validPoints[$lst]['passedAddress'] . "</li>\n";
            }
            echo "</ul>\n";
        }
    }

    /**
     * @function generateSideBar
     * @return nothing
     * @description Display a slidebar which generate dynamic links with Google Map
     */
    function generateSideBar()
    {
        $total = count($this->validPoints);
        $record = array();
        foreach ($this->validPoints as $key => $value)
        {
            $entry = array();

            // Here we recover all important informations
            $entry[] = '<a href="javascript:void(0);" >' . $value['passedAddress'] . '</a>';
            $entry[] = $value['title'];
            $record[] = $entry;
        }
        $this->sideBarEnabled = true;
        return $record;
    }

    /**
     * @static function generateIcon
     * @param $url (String) URL of the icon
     * @param $shadow (String) URL of the icon's shadow
     * @param $iconSize (Array (height, width)) icon's dimensions
     * @param $shadowSize (Array (height, width)) shadow icon's dimensions
     * @param $iconAnchor (Array (height, width)) Icon Anchor
     * @param $infoWindowAnchor (Array (height, width)) infoWindowAnchor
     * @return nothing
     * @description Generate javascript to make a marker icon
     */
    static function generateIcon($url, $shadow = null, $iconSize, $shadowSize = array('height' => 0, 'width' => 0), $iconAnchor = array('height' => 0, 'width' => 0), $infoWindowAnchor = array('height' => 5, 'width' => 5))
    {
        return "
		var gIcon = new GIcon();
		gIcon.image = \"" . $url . "\";
		gIcon.shadow = \"" . $shadow . ".\";
		gIcon.iconSize = new GSize(" . $iconSize['height'] . ", " . $iconSize['width'] . ");
		gIcon.shadowSize = new GSize(" . $shadowSize['height'] . "," . $shadowSize['width'] . ");
		gIcon.iconAnchor = new GLatLng(" . $iconAnchor['height'] . ", " . $iconAnchor['width'] . ");
		gIcon.infoWindowAnchor = new GLatLng(" . $infoWindowAnchor['height'] . ", " . $infoWindowAnchor['width'] . ");";
    }

    /**
     * @function showInvalidPoints
     * @param $displayType:string
     * @param $css_id:string
     * @returns nothing
     * @description Displays either a table or a list of the address points that are invalid.
     * Mainly used for debugging shows only the points that are NOT on the map
     */
    function showInvalidPoints($displayType, $css_id)
    {
        $total = count($this->invalidPoints);
        if ($displayType == "table")
        {
            echo "<table id=\"" . $css_id . "\">\n<tr>\n\t<td>Address</td>\n</tr>\n";
            for ($t = 0; $t < $total; $t++)
            {
                echo"<tr>\n\t<td>" . $this->invalidPoints[$t]['passedAddress'] . "</td>\n</tr>\n";
            }
            echo "</table>\n";
        }
        if ($displayType == "list")
        {
            echo "<ul id=\"" . $css_id . "\">\n";
            for ($lst = 0; $lst < $total; $lst++)
            {
                echo "<li>" . $this->invalidPoints[$lst]['passedAddress'] . "</li>\n";
            }
            echo "</ul>\n";
        }
    }

    /**
     * @function setWidth
     * @param $width:int
     * @returns nothing
     * @description Sets the width of the map to be displayed
     */
    function setWidth($width)
    {
        $this->mapWidth = $width;
    }

    /**
     * @function setHeight
     * @param $height:int
     * @returns nothing
     * @description Sets the height of the map to be displayed
     */
    function setHeight($height)
    {
        $this->mapHeight = $height;
    }

    /**
     * @function setAPIkey
     * @param $key:string
     * @returns nothing
     * @description Stores the API Key acquired from Google
     */
    function setAPIkey($key)
    {
        $this->apiKey = $key;
    }

    /**
     * @function printGoogleJS
     * @returns nothing
     * @description Adds the necessary Javascript for the Google Map to function
     * should be called in between the html <head></head> tags
     */
    function printGoogleJS()
    {
        echo "\n<script src=\"http://maps.google.com/maps?file=api&v=2&key=" . $this->apiKey . "\" type=\"text/javascript\"></script>\n";
    }

    /**
     * showMap
     * enhance the view of the map on a PHP Template
     * @return HTML Code
     */
    function showMap()
    {
        if (empty($this->validPoints))
        {
            return false;
        }

        echo "\n<div id=\"map\" style=\"width: " . $this->mapWidth . "px; height: " . $this->mapHeight . "px\">\n</div>\n";
        echo " <script type=\"text/javascript\">\n
		function showmap(){
		//<![CDATA[\n
		if (GBrowserIsCompatible()) {\n
		var map = new GMap2(document.getElementById(\"map\"));\n";
        if (empty($this->centerMap))
        {
            echo "map.setCenter(new GLatLng(" . $this->validPoints[0]['lat'] . "," . $this->validPoints[0]['long'] . "), " . $this->zoomLevel . ");\n";
        } else
        {
            echo $this->centerMap;
        }
        if (!empty($this->zoomLevel))
        {
            echo "map.setZoom(" . $this->zoomLevel . ");";
        }

        echo "}\n";
        if ($this->showControl)
        {
            foreach ($this->controlType as $key => $value)
            {
                if ($this->controlType[$key] == 'small')
                {
                    echo "map.addControl(new GSmallMapControl());\n";
                }
                if ($this->controlType[$key] == 'large')
                {
                    echo "map.addControl(new GLargeMapControl());\n";
                }
                if ($this->controlType[$key] == 'smallzoom')
                {
                    echo "map.addControl(new GSmallZoomControl());\n";
                }
                if ($this->controlType[$key] == 'smallzoom3d')
                {
                    echo "map.addControl(new GSmallZoomControl3D());\n";
                }
                if ($this->controlType[$key] == 'scale')
                {
                    echo "map.addControl(new GScaleControl());\n";
                }
                if ($this->controlType[$key] == 'large3d')
                {
                    echo "map.addControl(new GLargeMapControl3D());\n";
                }
                if ($this->controlType[$key] == 'navlabel')
                {
                    echo "map.addControl(new GNavLabelControl());\n";
                }
                if ($this->controlType[$key] == 'overview')
                {
                    echo "map.addControl(new GOverviewMapControl());\n";
                }
                if ($this->controlType[$key] == 'hierarchical')
                {
                    echo "map.addControl(new GHierarchicalMapTypeControl());\n";
                }
            }
        }

        if ($this->showType)
        {
            echo "map.addControl(new GMapTypeControl());\n";
        }

        $numPoints = count($this->validPoints);

        // Getting the sidebar
        echo "var sideBar = document.getElementById(\"sideBar\");";

        for ($g = 0; $g < $numPoints; $g++)
        {

            // Custom icon generated if declare
            if ($this->validPoints[$g]['icon'])
            {
                echo $this->validPoints[$g]['icon'];
            } else
            {
                echo $this->defaultIcon;
            }
            echo "var point" . $g . " = new GLatLng(" . $this->validPoints[$g]['lat'] . "," . $this->validPoints[$g]['long'] . ")\n;
var marker" . $g . " = new GMarker(point" . $g . ", {draggable: true, icon:gIcon});\n
map.addOverlay(marker" . $g . ")\n
var gInfoMarker" . $g . " = \"" . $this->validPoints[$g]['htmlMessage'] . "\" \n;
var gTitle" . $g . " = \"" . $this->validPoints[$g]['title'] . "\" \n";


            // Sidebar generation if declare
            if ($this->sideBarEnabled == true)
            {
                echo "
		var newLabel = document.createElement(\"div\");
		newLabel.innerHTML = \"<a id=marker" . $g . " href=# > \" + gTitle" . $g . " + \" </a> \";
		sideBar.appendChild (newLabel);
		var marker = document.getElementById (\"marker" . $g . "\");
		marker.onclick = function(){GEvent.trigger(marker" . $g . ",'click'); return false};
		marker.onmouseover = function() {GEvent.trigger(marker" . $g . ", 'mouseover'); return false};
		marker.onmouseout = function() {GEvent.trigger(marker" . $g . ", 'mouseout'); return false};
	\n
	";
            }

            // Open WindowInformation without actions
            if ($this->openWindowInformation == true)
            {
                echo "marker" . $g . ".openInfoWindowHtml(gInfoMarker" . $g . ");\n";
            }

            // Run the action defined
            echo "GEvent.addListener(marker" . $g . ", \"click\", function() {\n";
            if ($this->validPoints[$g]['htmlMessage'] != null)
            {
                if ($this->actionClick["type"] == "link")
                {
                    echo 'window.parent.location.replace("' . $this->actionClick["url"] . '")';
                }

                if ($this->actionClick["type"] == "openInfoWindow")
                {
                    echo "marker" . $g . ".openInfoWindowHtml(gInfoMarker" . $g . ");\n";
                }
            }

            echo "});\n";
            if ($this->openWindowInformation !== true)
            {
                // Execute l'action définie lorsqu'on survole un marqueur
                echo "GEvent.addListener(marker" . $g . ", \"mouseover\", function() {\n";
                if ($this->validPoints[$g]['htmlMessage'] != null)
                {
                    if ($this->actionOver['type'] == "openInfoWindow")
                    {
                        echo "marker" . $g . ".openInfoWindowHtml(gInfoMarker" . $g . ");\n";
                    }
                }

                echo "});\n";


                // Execute the action when the mouse is over a marker
                echo "GEvent.addListener(marker" . $g . ", \"mouseout\", function() {\n";
                if ($this->validPoints[$g]['htmlMessage'] != null)
                {
                    if ($this->actionOver['type'] == "openInfoWindow")
                    {
                        echo "marker" . $g . ".closeInfoWindow();\n";
                    }
                }
                echo "});\n";
            }

            // Execute an action when the user drag & drop a marker
            if ($this->dragndrop == true)
            {
                echo "GEvent.addListener(marker" . $g . ", \"dragstart\", function () {\n";

                echo "});\n";
                echo "GEvent.addListener(marker" . $g . ", \"dragend\", function() {
          	
          });";
            }
        }

        echo " //]]>\n
		}";
        // Display the map
        echo "window.onload = showmap;";

        echo "</script>\n";
    }

    /**
     * XML Parsing Engine
     * @param string $xml
     * @return array $xmlarray
     */
    function xml2array($xml)
    {
        $this->depth = -1;
        $this->xml_parser = xml_parser_create();
        xml_set_object($this->xml_parser, $this);
        xml_parser_set_option($this->xml_parser, XML_OPTION_CASE_FOLDING, 0); //Don't put tags uppercase
        xml_set_element_handler($this->xml_parser, "startElement", "endElement");
        xml_set_character_data_handler($this->xml_parser, "characterData");
        xml_parse($this->xml_parser, $xml, true);
        xml_parser_free($this->xml_parser);
        return $this->arrays[0];
    }

    /**
     * XML Parsing Method StartElement
     * @param $parser
     * @param $name
     * @param $attrs
     * @return unknown_type
     */
    function startElement($parser, $name, $attrs)
    {
        $this->keys[] = $name;
        $this->attrs[] = $attrs;
        $this->node_flag = 1;
        $this->depth++;
    }

    /**
     * characterData
     * @param $parser
     * @param $data
     * @return unknown_type
     */
    function characterData($parser, $data)
    {
        if (!trim($data))
            return;
        $key = end($this->keys);
        $this->arrays[$this->depth][$key] = $data;
        $this->node_flag = 0;
    }

    /**
     * endElement
     * @param $parser
     * @param $name
     * @return unknown_type
     */
    function endElement($parser, $name)
    {
        $key = array_pop($this->keys);
        if ($this->node_flag == 1)
        {
            $this->arrays[$this->depth][$key] = $this->arrays[$this->depth + 1];

            $this->arrays[$this->depth + 1] = null;
            // unset($this->arrays[$this->depth+1]);
        }

        $this->node_flag = 1;
        $this->depth--;
    }

}

?>
