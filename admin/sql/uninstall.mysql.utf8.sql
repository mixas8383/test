-- @version		$Id: script.php 63 2011-04-27 01:35:59Z bfoecke $
-- @package		Goodpractice
-- @subpackage	Component
-- @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
-- @author		Benjamin F�cke
-- @link		http://www.veasy.de
-- @license		http://www.gnu.org/licenses/gpl-2.0.html

DROP TABLE IF EXISTS `#__goodpractice_entry`;
-- DROP TABLE IF EXISTS `#__goodpractice_comments`;
DROP TABLE IF EXISTS `#__goodpractice_entry_categories`;
DROP TABLE IF EXISTS `#__goodpractice_entry_links`;
DROP TABLE IF EXISTS `#__goodpractice_entry_pdfs`;
DROP TABLE IF EXISTS `#__goodpractice_entry_tags`;

