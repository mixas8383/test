<?php

/**
 * @version		$Id: script.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Themensammlung
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Script file of Themensammlung component
 */
class com_goodpracticeInstallerScript
{

    protected $old_version;

    /**
     * method to install the component
     *
     * @param	JInstallerComponent	$parent	The class calling this method
     *
     * @return void
     */
    function install($parent)
    {
        
    }

    /**
     * method to uninstall the component
     *
     * @param	JInstallerComponent	$parent	The class calling this method
     *
     * @return void
     */
    function uninstall($parent)
    {
        // Get the DB object
        $db = JFactory::getDbo();

        // Delete all categories
        $query = $db->getQuery(true);
        $query->delete();
        $query->from('#__categories');
        $query->where('extension=' . $db->quote('com_goodpractice'));
        $db->setQuery($query);
        $db->execute();
       
        $table = JTable::getInstance('Category');
        $table->rebuild();
    }

    /**
     * method to update the component
     *
     * @param	JInstallerComponent	$parent	The class calling this method
     *
     * @return void
     */
    function update($parent)
    {
        // Get the new version
        $new_version = $parent->getParent()->getManifest()->version;

        if ($this->old_version && $this->old_version == '0.0.1' && version_compare($new_version, '0.0.1') > 0)
        {
            // Get the DB object
            $db = JFactory::getDbo();

            // Get all entry tables
            $query = 'SHOW TABLES LIKE ' . $db->quote($db->getPrefix() . 'goodpractice_entry_%');
            $db->setQuery($query);
            $tables = $db->loadColumn();

          

            // Update all entry tables
            if (!empty($tables))
            {
                foreach ($tables as $table)
                {
                    $query = 'ALTER TABLE `' . $table . '` DROP INDEX `idx_alternate`, ADD INDEX `idx_alternate` (`alternate`)';
                    $db->setQuery($query);
                    $db->execute();

                   
                }
            }
        }
    }

    /**
     * method to run before an install/update/discover_install method
     *
     * @param	JInstallerComponent	$parent	The class calling this method
     * @param 	string				$type  	The type of change (install, update or discover_install)
     *
     * @return void
     */
    function preflight($type, $parent)
    {
        if ($type == 'update')
        {
            // Get the DB object
            $db = JFactory::getDbo();

            // Get a new query
            $query = $db->getQuery(true);
            $query->select('s.version_id');
            $query->from('#__extensions AS e');
            $query->leftJoin('#__schemas AS s ON e.extension_id=s.extension_id');
            $query->where('e.element=' . $db->quote('com_goodpractice'));
            $db->setQuery($query);
            $this->old_version = $db->loadResult();
        }
    }

    /**
     * method to run after an install/update/discover_install method
     *
     * @param	JInstallerComponent	$parent	The class calling this method
     * @param 	string				$type  	The type of change (install, update or discover_install)
     *
     * @return void
     */
    function postflight($type, $parent)
    {
        // Redirect to the component main view after installing
    }

}
