<?php

/**
 * @version		$Id: entry.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Goodpractice
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke.
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access
defined('_JEXEC') or die('Restricted access');

// import Joomla table library
jimport('joomla.database.table');

use Joomla\CMS\Application\ApplicationHelper;

/**
 * Entry Table class of Goodpractice component
 *
 * @since	0.0.1
 */
class GoodpracticeTableEntry extends JTable
{

    /**
     * Constructor
     *
     * @param	JDatabase	$db	Database connector object
     *
     * @since	0.0.1
     *
     * @see		JTable::__construct
     */
    public function __construct(&$db)
    {
        parent::__construct('#__goodpractice_entry', 'id', $db);
    }

    /**
     * Method to compute the default name of the asset.
     * The default name is in the form `table_name.id`
     * where id is the value of the primary key of the table.
     *
     * @return	string
     *
     * @since	0.0.1
     *
     * @see		JTable::_getAssetName
     */
    protected function _getAssetName()
    {
        $k = $this->_tbl_key;
        return 'com_goodpractice.entry.' . (int) $this->$k;
    }

    /**
     * Method to return the title to use for the asset table.
     *
     * @return	string
     *
     * @since	0.0.1
     *
     * @see		JTable::_getAssetTitle
     */
    protected function _getAssetTitle()
    {
        return $this->title;
    }

    /**
     * Get the parent asset id for the record
     *
     * @return	int
     *
     * @since	0.0.1
     *
     * @see		JTable::_getAssetParentId
     */
    protected function _getAssetParentId(Joomla\CMS\Table\Table $table = null, $id = null)
    {
        // Initialise variables.
        $assetId = null;
        $db = $this->getDbo();

        // This is a entry under a category.
        if ($this->catid)
        {
            // Build the query to get the asset id for the parent category.
            $query = $db->getQuery(true);
            $query->select('asset_id');
            $query->from('#__categories');
            $query->where('id = ' . (int) $this->catid);
        } else
        {
            // Build the query to get the asset id for the com_goodpractice component.
            $query = $db->getQuery(true);
            $query->select('id');
            $query->from('#__assets');
            $query->where('name = ' . $db->quote('com_goodpractice'));
        }

        // Get the asset id from the database.
        $this->_db->setQuery($query);
        if ($result = $this->_db->loadResult())
        {
            return (int) $result;
        } else
        {
            return parent::_getAssetParentId($table, $id);
        }
    }

    /**
     * Method to bind an associative array or object to the JTable instance.This
     * method only binds properties that are publicly accessible and optionally
     * takes an array of properties to ignore when binding.
     *
     * @param	mixed	An associative array or object to bind to the JTable instance.
     * @param	mixed	An optional array or space separated list of properties
     * 					to ignore while binding.
     *
     * @return	boolean	True on success.
     *
     * @since	0.0.1
     *
     * @see		JTable::bind
     */
    public function bind($array, $ignore = '')
    {


        // Bind the params
        if (isset($array['params']) && is_array($array['params']))
        {
            $registry = new JRegistry();
            $registry->loadArray($array['params']);
            $array['params'] = (string) $registry;
        }

        // Bind the metadata
        if (isset($array['metadata']) && is_array($array['metadata']))
        {
            $registry = new JRegistry();
            $registry->loadArray($array['metadata']);
            $array['metadata'] = (string) $registry;
        }

        // Set the alias
        if ((!isset($array['alias']) || empty($array['alias'])) && isset($array['title']))
        {
            $array['alias'] = $array['title'];
        }

        // Bind the rules.
        if (isset($array['rules']) && is_array($array['rules']))
        {
            $rules = new JRules($array['rules']);
            $this->setRules($rules);
        }


        return parent::bind($array, false);
    }

    /**
     * Method to store a row in the database from the JTable instance properties.
     * If a primary key value is set the row with that primary key value will be
     * updated with the instance property values.  If no primary key value is set
     * a new row will be inserted into the database with the properties from the
     * JTable instance.
     *
     * @param	boolean True to update fields even if they are null.
     *
     * @return	boolean	True on success.
     *
     * @since	0.0.1
     *
     * @see		JTable::store
     */
    public function store($updateNulls = false)
    {



        $date = JFactory::getDate();
        $user = JFactory::getUser();
        $db = JFactory::getDbo();
        if (empty($this->author))
        {
            $this->author = 0;
            $this->author_alias = '';
        }

        if (empty($this->author))
        {
            $this->author = 0;
            $this->author_alias = '';
        }

        if (empty($this->params))
        {
            $this->params = '';
        }
        if (empty($this->introduction))
        {
            $this->introduction = 0;
        }
        if (empty($this->text2))
        {
            $this->text2 = '';
        }
        if (empty($this->laufzeit_von))
        {
            $this->laufzeit_von = '';
        }
        if (empty($this->laufzeit_bis))
        {
            $this->laufzeit_bis = '';
        }
        if (empty($this->project_status))
        {
            $this->project_status = '';
        }

        if (empty($this->hochschule))
        {
            $this->hochschule = '';
        }
        if (empty($this->projektart))
        {
            $this->projektart = '';
        }
        if (empty($this->region))
        {
            $this->region = '';
        }
        if (empty($this->comment))
        {
            $this->comment = '';
        }
        if (empty($this->contact_fax))
        {
            $this->contact_fax = '';
        }
        if (empty($this->contact_web2))
        {
            $this->contact_web2 = '';
        }
        if (empty($this->newTags))
        {
            $this->newTags = '';
        }
        if (empty($this->contact_traeger))
        {
            $this->contact_traeger = '';
        }


        if ($this->id)
        {
            // Existing item
            $this->modified = $date->toSQL();
            $this->modified_by = $user->get('id');
            $table = JTable::getInstance('Entry', 'GoodpracticeTable');
            $table->load($this->id);
            $change_ordering = $table->catid != $this->catid;
        } else
        {
            // New entry. An entry created and created_by field can be set by the user,
            // so we don't touch either of these if they are set.

            if (!intval($this->created))
            {
                $this->created = $date->toSQL();
                $this->modified = $date->toSQL(); // changes made for Goodpractice lastmodified!
            }
            if (empty($this->created_by))
            {
                $this->created_by = $user->get('id');
                $this->modified_by = $user->get('id'); // changes made for Goodpractice lastmodified!
            }
            $change_ordering = true;
        }
        if ($change_ordering)
        {
            // Set the ordering to be at the end of the catagory
            $query = $db->getQuery(true);
            $query->select('MAX(ordering)');
            $query->from('#__goodpractice_entry');
            $query->where('catid=' . (int) $this->catid);
            $db->setQuery($query);
            $max = $db->loadResult();
            $this->ordering = $max + 1;
        }

        // Attempt to store the data.
        if (parent::store($updateNulls))
        {
            if (isset($table))
            {
                $table->reorder('catid=' . $table->catid);
            }
            return true;
        }



        return false;
    }

    /**
     * Method to perform sanity checks on the JTable instance properties to ensure
     * they are safe to store in the database.  Child classes should override this
     * method to make sure the data they are storing in the database is safe and
     * as expected before storage.
     *
     * @return	boolean	True if the instance is sane and able to be stored in the database.
     *
     * @since	0.0.1
     *
     * @see		JTable::check
     */
    function check()
    {
        // Sanitize title
        $this->title = trim($this->title);
        if ($this->title == '')
        {
            $this->setError(JText::_('COM_GOODPRACTICE_ERROR_EMPTY_TITLE'));
            return false;
        }

        // Sanitize alias
        if (trim($this->alias) == '')
        {
            $this->alias = $this->title;
        }
        $app = JFactory::getApplication();
        $this->alias = ApplicationHelper::stringURLSafe($this->alias);
        if (trim(str_replace('-', '', $this->alias)) == '')
        {
            $this->alias = JFactory::getDate()->format('Y-m-d-H-i-s');
        }

        // Verify that the alias is unique
        $table = JTable::getInstance('Entry', 'GoodpracticeTable');
        if ($table->load(array('alias' => $this->alias, 'catid' => $this->catid)) && ($table->id != $this->id || $this->id == 0))
        {
            $this->setError(JText::_('COM_GOODPRACTICE_ERROR_UNIQUE_ALIAS'));
            return false;
        }

        // check for valid category
        if (trim($this->catid) == '')
        {
            $this->setError(JText::_('COM_GOODPRACTICE_ERROR_EMPTY_CATEGORY'));
            return false;
        }
        return true;
    }

}
