<?php

/**
 * @version		$Id: goodpractice.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Themensammluing
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Hofmann Büroorganisation. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
defined('_JEXEC') or die('Restricted access');

// import helper files
require_once dirname(__FILE__) . '/helpers.php';

// import joomla controller library
jimport('joomla.application.component.controller');

// Get an instance of the controller prefixed by Goodpractice
$controller = JControllerLegacy::getInstance('Goodpractice');

// Perform the Request task
$controller->execute(JFactory::getApplication()->input->get('task'));

// Redirect if set by the controller
$controller->redirect();

