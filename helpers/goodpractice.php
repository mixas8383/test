<?php

/**
 * @version		$Id: goodpractice.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Goodpractice
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Goodpractice component helper.
 *
 * @since	0.0.1
 */
class GoodpracticeHelper extends JHelperContent
{

    public static $extension = 'com_goodpractice';

    /**
     * Configure the Linkbar.
     *
     * @param	string	$submenu	the name of the current submenu
     *
     * @return void
     *
     * @since	0.0.1
     */
    public static function addSubmenu($vName)
    {
        // Addsubmenu            
        JHtmlSidebar::addEntry(
                JText::_('COM_GOODPRACTICE_SUBMENU_ENTRIES'), 'index.php?option=com_goodpractice&view=entries', $vName == 'entries');
        JHtmlSidebar::addEntry(
                JText::_('COM_GOODPRACTICE_SUBMENU_CATEGORIES'), 'index.php?option=com_categories&view=categories&extension=com_goodpractice', $vName == 'categories');
    }

    /**
     * Gets a list of the actions that can be performed.
     *
     * @param	int		$categoryId	The category ID.
     * @param	int		$entryId	The entry ID.
     *
     * @return	JObject
     *
     * @since	0.0.1
     */
    public static function getActions($categoryId = 0, $entryId = 0, $t = false)
    {
        // Get the current user
        $user = JFactory::getUser();

        // Prepare the result
        $result = new JObject;
        if (empty($entryId))
        {
            if (empty($categoryId))
            {
                $assetName = 'com_goodpractice';
            } else
            {
                $assetName = 'com_goodpractice.category.' . (int) $categoryId;
            }
        } else
        {
            $assetName = 'com_goodpractice.entry.' . (int) $entryId;
        }
        $actions = array('core.admin', 'core.manage', 'core.create', 'core.edit', 'core.edit.own', 'core.edit.state', 'core.delete');
        foreach ($actions as $action)
        {
            // Get the authorised actions
            $result->set($action, $user->authorise($action, $assetName));
        }
        return $result;
    }

    /**
     * Create title tables for dealing with different language collation
     *
     * @param	$array	$array	an array of language tags
     *
     * @return void
     *
     * @since	0.0.1
     */
    public static function createTitleTables($tags = array())
    {
        // Get the DB object
        $db = JFactory::getDBO();

        // Loop for each tags
        foreach ($tags as $tag)
        {
            // Get the collation for this tag
            $collation = self::getCollation($tag);

            // Get the table name using the DB prefix
            $name = $db->getPrefix() . 'goodpractice_entry_tags';

            // Get information about this table
            $query = 'SHOW TABLE STATUS WHERE Name=' . $db->quote($name);
            $db->setQuery($query);
            $table = $db->loadObject();
            if (!$table)
            {
                // Table is not found
                $create = 'CREATE TABLE `' . $name . '` (';
                $create .= '		`eid`		integer unsigned	NOT NULL COMMENT ' . $db->quote('FK to the #__goodpractice_entry table') . ',';
                $create .= '		`alternate`	varchar(255)		NOT NULL COLLATE ' . $collation . ',';
                $create .= '		`ordering`	integer				NOT NULL default 0,';
                $create .= '		KEY 	`idx_eid`	(`eid`),';
                $create .= '		KEY		`idx_alternate`	(`alternate`),';
                $create .= '		KEY 	`idx_ordering`	(`ordering`)';
                $create .= ') COLLATE ' . $collation;
                $db->setQuery($create);
                $db->execute();
            } elseif ($collation != $table->Collation)
            {
                // Table is found and current collation is not correct
                $alter = 'ALTER TABLE `' . $name . '` CHARACTER SET utf8 COLLATE ' . $collation;
                $db->setQuery($alter);
                $db->execute();
                $alter = 'ALTER TABLE `' . $name . '` MODIFY `alternate` varchar(255) NOT NULL COLLATE ' . $collation;
                $db->setQuery($alter);
                $db->execute();
            }
        }
    }

    /**
     * Get the collation for a given language tag
     *
     * @param	string	$tag	A language tag
     *
     * @return string	The collation used
     *
     * @since	0.0.1
     */
    public static function getCollation($tag)
    {
        // In J!1.7, mysql collation must be part of the xml language description file
        // In J!1.6, get the collation from the language ini files

        $lang = JLanguage::getInstance(false);
        $lang->load('com_goodpractice', JPATH_ADMINISTRATOR, null, false, false) || $lang->load('com_goodpractice', JPATH_ADMINISTRATOR, $lang->getDefault(), false, false);
        $collation = $lang->get('collation', $lang->_('COM_GOODPRACTICE_MYSQL_COLLATION'));

        // Get the DB object
        $db = JFactory::getDBO();

        // Get collation description
        $query = 'SHOW COLLATION WHERE Collation=' . $db->quote($collation);
        $db->setQuery($query);
        $result = $db->loadObject();

        // If the collation has not been found
        if (empty($result))
        {
            // Fallback to default collation
            $collation = 'utf8_unicode_ci';
        }
        return $collation;
    }

    /**
     * Applies the content tag filters to arbitrary text as per settings for current user group
     *
     * @param   text  $text  The string to filter
     *
     * @return  string  The filtered string
     *
     * @deprecated  4.0  Use JComponentHelper::filterText() instead.
     */
    public static function filterText($text)
    {
        JLog::add('GoodpracticeHelper::filterText() is deprecated. Use JComponentHelper::filterText() instead.', JLog::WARNING, 'deprecated');

        return JComponentHelper::filterText($text);
    }

}
