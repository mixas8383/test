<?php

/**
 * @version		$Id: icon.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Goodpractice
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * Goodpractice Component Html Helper
 *
 * @since	0.0.1
 */
abstract class JHtmlIcon
{

    /**
     * Display an email icon for the entry.
     *
     * @param	object	$entry	The entry in question.
     * @param	object	$params		The entry parameters
     * @param	array	$attribs	Not used??
     *
     * @return	string	The HTML for the entry edit icon.
     *
     * @since	0.0.1
     */
    static function email($entry, $params, $attribs = array())
    {
        $uri = JURI::getInstance();
        $base = $uri->toString(array('scheme', 'host', 'port'));
        $template = JFactory::getApplication()->getTemplate();
        $link = $base . JRoute::_(GoodpracticeHelperRoute::getEntryRoute($entry->id, $entry->catid), false);
        $url = 'index.php?option=com_mailto&tmpl=component&template=' . $template . '&link=' . base64_encode($link);
        $status = 'width=400,height=350,menubar=yes,resizable=yes';
        if ($params->get('show_icons', 1))
        {
            $text = JHtml::_('image', 'system/emailButton.png', JText::_('JGLOBAL_EMAIL'), NULL, true);
        } else
        {
            $text = '&#160;' . JText::_('JGLOBAL_EMAIL');
        }
        $attribs['title'] = JText::_('JGLOBAL_EMAIL');
        $attribs['onclick'] = "window.open(this.href,'win2','" . $status . "'); return false;";
        $output = JHtml::_('link', JRoute::_($url), $text, $attribs);
        return $output;
    }

    /**
     * Display an edit icon for the entry.
     *
     * This icon will not display in a popup window, nor if the entry is trashed.
     * Edit access checks must be performed in the calling code.
     *
     * @param	object	$entry	The entry in question.
     * @param	object	$params		The entry parameters
     * @param	array	$attribs	Not used??
     *
     * @return	string	The HTML for the entry edit icon.
     *
     * @since	0.0.1
     */
    static function edit($entry, $params, $attribs = array())
    {
        // Initialise variables.
        $uri = JFactory::getURI();
        $url = 'index.php?option=com_goodpractice&task=entry.edit&id=' . $entry->id . '&return=' . base64_encode($uri);
        $text = JHtml::_('image', 'system/edit.png', JText::_('JGLOBAL_EDIT'), NULL, true);
        $attribs['title'] = JText::_('JGLOBAL_EDIT');
        return JHtml::_('link', JRoute::_($url), $text, $attribs);
    }

    /**
     * Display a print icon for the entry.
     *
     * @param	object	$entry	The entry in question.
     * @param	object	$params		The entry parameters
     * @param	array	$attribs	Not used??
     *
     * @return	string	The HTML for the entry edit icon.
     *
     * @since	0.0.1
     */
    static function printPopup($entry, $params, $attribs = array())
    {
        $url = GoodpracticeHelperRoute::getEntryRoute($entry->id, $entry->catid);
        $url .= '&tmpl=component&print=1&layout=default';
        $status = 'status=no,toolbar=no,scrollbars=yes,titlebar=no,menubar=no,resizable=yes,width=640,height=480,directories=no,location=no';

        // checks template image directory for image, if non found default are loaded
        if ($params->get('show_icons', 1))
        {
            $text = JHtml::_('image', 'system/printButton.png', JText::_('JGLOBAL_PRINT'), NULL, true);
        } else
        {
            $text = JText::_('JGLOBAL_ICON_SEP') . '&#160;' . JText::_('JGLOBAL_PRINT') . '&#160;' . JText::_('JGLOBAL_ICON_SEP');
        }
        $attribs['title'] = JText::_('JGLOBAL_PRINT');
        $attribs['onclick'] = "window.open(this.href,'win2','" . $status . "'); return false;";
        $attribs['rel'] = 'nofollow';
        return JHtml::_('link', JRoute::_($url), $text, $attribs);
    }

    /**
     * Display a "real" print icon for the entry.
     *
     * @param	object	$entry	The entry in question.
     * @param	object	$params		The entry parameters
     * @param	array	$attribs	Not used??
     *
     * @return	string	The HTML for the entry edit icon.
     *
     * @since	0.0.1
     */
    static function printScreen($entry, $params, $attribs = array())
    {
        // checks template image directory for image, if non found default are loaded
        if ($params->get('show_icons', 1))
        {
            $text = JHtml::_('image', 'system/printButton.png', JText::_('JGLOBAL_PRINT'), NULL, true);
        } else
        {
            $text = JText::_('JGLOBAL_ICON_SEP') . '&#160;' . JText::_('JGLOBAL_PRINT') . '&#160;' . JText::_('JGLOBAL_ICON_SEP');
        }
        return '<a href="#" onclick="window.print();return false;">' . $text . '</a>';
    }

    static function prev($sibling, $params, $attribs = array())
    {

        // checks template image directory for image, if non found default are loaded
        $text = JHtml::_('image', 'system/arrow_rtl.png', $sibling->title, array('title' => $sibling->title), true);

        return '<a href="' . JRoute::_(GoodpracticeHelperRoute::getCategoryRoute($sibling->id)) . '" >' . $text . '</a>';
    }

    static function next($sibling, $params, $attribs = array())
    {

        // checks template image directory for image, if non found default are loaded
        $text = JHtml::_('image', 'system/arrow.png', $sibling->title, array('title' => $sibling->title), true);

        return '<a href="' . JRoute::_(GoodpracticeHelperRoute::getCategoryRoute($sibling->id)) . '" >' . $text . '</a>';
    }

}
