<?php

/**
 * @version		$Id: category.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Goodpractice
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import joomla categories library
jimport('joomla.application.categories');

/**
 * Goodpractice Component Category Tree
 *
 * @since	0.0.1
 */
class GoodpracticeCategories extends JCategories
{

    /**
     * Class constructor
     *
     * @param	array	$options	array of options passed to parent class
     *
     * @since	0.0.1
     *
     * @see		JCategories::__construct
     */
    public function __construct($options = array())
    {
        $options['table'] = '#__goodpractice_entry';
        $options['extension'] = 'com_goodpractice';
        $options['statefield'] = 'published';
        parent::__construct($options);
    }

}
