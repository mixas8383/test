<?php

/**
 * @version		$Id: route.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Goodpractice
 * @subpackage	Component
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved.
 * @author		Benjamin F�cke
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

// import joomla component helper library
jimport('joomla.application.component.helper');

// import joomla categories library
jimport('joomla.application.categories');

/**
 * Goodpractice Component Route Helper
 *
 * @since	0.0.1
 */
abstract class GoodpracticeHelperRoute
{

    protected static $lookup;

    /**
     * @param	int	$id		The id:slug of the entry item
     * @param	int	$catid	The id of the category
     *
     * @since	0.0.1
     *
     * @return	URL			The route of the entry item
     */
    public static function getEntryRoute($id, $catid = 0)
    {
        $needles = array('entry' => array((int) $id));

        //Create the link
        $link = 'index.php?option=com_goodpractice&view=item&id=' . $id;
        if ((int) $catid > 1)
        {
            $categories = JCategories::getInstance('Goodpractice', array('access' => false));
            $category = $categories->get((int) $catid);
            if ($category)
            {
                $needles['category'] = array_reverse($category->getPath());
                $needles['categories'] = $needles['category'];
                $link .= '&catid=' . $catid;
            }
        }
        if ($item = self::_findItem($needles))
        {
            $link .= '&Itemid=' . $item;
        } elseif ($item = self::_findItem())
        {
            $link .= '&Itemid=' . $item;
        }
        return $link;
    }

    /**
     * @param	int	$catid	The id of the category
     *
     * @since	0.0.1
     *
     * @return	URL			The route of the category
     */
    public static function getCategoryRoute($catid)
    {
        if ($catid instanceof JCategoryNode)
        {
            $id = $catid->id;
            $category = $catid;
        } else
        {
            $id = (int) $catid;
            $category = JCategories::getInstance('Goodpractice', array('access' => false))->get($id);
        }
        $needles = array('category' => array($id));
        if ($item = self::_findItem($needles))
        {
            $link = 'index.php?Itemid=' . $item;
        } else
        {
            //Create the link
            $link = 'index.php?option=com_goodpractice&view=category&id=' . $id;
            if ($category)
            {
                $catids = array_reverse($category->getPath());
                $needles = array('category' => $catids, 'categories' => $catids);
                if ($item = self::_findItem($needles))
                {
                    $link .= '&Itemid=' . $item;
                } elseif ($item = self::_findItem())
                {
                    $link .= '&Itemid=' . $item;
                }
            }
        }
        return $link;
    }

    /**
     * @param	int	$id		The id:slug of the entry item
     *
     * @since	0.0.1
     *
     * @return	URL			The route of the form
     */
    public static function getFormRoute($id)
    {
        //Create the link
        if ($id)
        {
            $link = 'index.php?option=com_goodpractice&task=entry.edit&id=' . $id;
        } else
        {
            $link = 'index.php?option=com_goodpractice&task=entry.edit&id=0';
        }
        return $link;
    }

    protected static function _findItem($needles = null)
    {
        $option = 'com_goodpractice';
        $app = JFactory::getApplication();
        $menus = $app->getMenu('site');

        // Prepare the reverse lookup array.
        if (self::$lookup === null)
        {
            self::$lookup = array();
            $component = JComponentHelper::getComponent($option);
            $items = $menus->getItems('component_id', $component->id);
            if ($items)
            {
                foreach ($items as $item)
                {
                    if (isset($item->query) && isset($item->query['view']))
                    {
                        $view = $item->query['view'];
                        if (!isset(self::$lookup[$view]))
                        {
                            self::$lookup[$view] = array();
                        }
                        if (isset($item->query['id']))
                        {
                            self::$lookup[$view][$item->query['id']] = $item->id;
                        }
                    }
                }
            }
        }
        if ($needles)
        {
            foreach ($needles as $view => $ids)
            {
                if (isset(self::$lookup[$view]))
                {
                    foreach ($ids as $id)
                    {
                        if (isset(self::$lookup[$view][(int) $id]))
                        {
                            return self::$lookup[$view][(int) $id];
                        }
                    }
                }
            }
        } else
        {
            $active = $menus->getActive();
            if ($active && $active->component == $option)
            {
                return $active->id;
            }
        }
        return null;
    }

    /**
     * @param	obj	$cat		The current category to be displayed next in the sitemap
     *
     * @since	0.0.1
     *
     * @return	string			next html list-element in sitemap with link
     */
    public static function showThemenSitemap($cat)
    {
        $str = '';
        // root-category should not be displayed..
        if ($cat->level > 0)
        {

            // call function in order to get the link of the current category
            $lnk = GoodpracticeHelperRoute::getCategoryRoute($cat->id);

            // call function in order to count the number of items of the current category
            $cnt = self::getItemCount($cat);

            // build html-code to be displayed in the sitemap
            $sizestyle = 18 - (int) ($cat->level) * 1.5; // font-size depends on category level
            $str = "<li type='square' style='margin-top:" . (18 - (int) ($cat->level) * 4) . "px; font-size:" . $sizestyle . "px'><span class='item-title'><a href='" . JRoute::_($lnk) . "'>" . $cat->title;
            if ($cnt != 0)
            {
                $str .= " (" . $cnt . ")";
            }
            $str .= "</a></span></li><ul>";
        }

        // recursive calling of this function for each child of current category
        $children = $cat->getChildren();
        foreach ($children as $child)
        {
            $str .= self::showThemenSitemap($child);
        }

        // completing and returning html-code to be displayed in the sitemap
        $str .= "</ul>";
        return $str;
    }

    /**
     * @param	obj	$cat		The current category
     *
     * @since	0.0.1
     *
     * @return	int			number of items in the current category
     */
    public static function getItemCount($cat)
    {

        // preparing database query
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        // building sql query to count items by alternativecatid
        $query->select('COUNT(*)');
        $query->from('#__goodpractice_entry');
        $query->join('LEFT', '#__goodpractice_entry_categories ON #__goodpractice_entry.id = #__goodpractice_entry_categories.eid');
        $query->where('#__goodpractice_entry_categories.alternativecatid = ' . (int) $cat->id . ' AND #__goodpractice_entry.published = 1');

        // set query and store result
        $db->setQuery($query);
        $cnt1 = $db->loadResult();

        $query = $db->getQuery(true);

        // new sql query to count items by catid
        $query->select('COUNT(*)');
        $query->from('#__goodpractice_entry');
        $query->where('#__goodpractice_entry.catid = ' . (int) $cat->id . ' AND #__goodpractice_entry.published = 1');

        // set query and store result
        $db->setQuery($query);
        $cnt2 = $db->loadResult();

        // getting total result by adding of the 2 sub-results
        $cnt = $cnt1 + $cnt2;



     

        return($cnt);
    }

    /**
     * @param	obj	$entryID	The ID of the entry
     *
     * @since	0.0.1
     *
     * @return	result			The contact data (name, image, id)
     */
    public static function getContactData($entryID)
    {

        // preparing database query
        $db = JFactory::getDbo();
        $query = $db->getQuery(true);

        $query->select('name, image, #__contact_details.id');
        $query->from('#__goodpractice_entry');
        $query->join('LEFT', '#__contact_details ON #__goodpractice_entry.author = #__contact_details.id');
        $query->where('#__goodpractice_entry.id = ' . $entryID);

        // set query and store result
        $db->setQuery($query);
        $result = $db->loadAssoc();


        return($result);
    }

}
