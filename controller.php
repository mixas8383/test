<?php

/**
 * @version		$Id: controller.php 63 2011-04-27 01:35:59Z bfoecke $
 * @package		Goodpractice
 * @subpackage	Module Latest
 * @copyright	Copyright (C) 2010-2011 Benjamin F�cke. All rights reserved. 
 * @author		Benjamin F�cke  
 * @link		http://www.veasy.de
 * @license		http://www.gnu.org/licenses/gpl-2.0.html 
 */
// No direct access to this file
defined('_JEXEC') or die('Restricted access');

/**
 * General Controller of Goodpractice component
 *
 * @since	0.0.1
 */
class GoodpracticeController extends JControllerLegacy
{

    /**
     * @var		string	The default view for the display method.
     *
     * @since	0.0.1
     *
     * @see		JController::$default_view
     */
    protected $default_view = 'entries';

    /**
     * Typical view method for MVC based architecture
     *
     * @param	boolean			If true, the view output will be cached
     * @param	array			An array of safe url parameters and their variable types, for valid values see {@link JFilterInput::clean()}.
     *
     * @return	JController		This object to support chaining.
     *
     * @since	0.0.1
     *
     * @see		JController::display
     */
    public function display($cachable = false, $urlparams = false)
    {
        $view = $this->input->get('view', 'entries');
        $layout = $this->input->get('layout', 'entries');
        $id = $this->input->getInt('id');

        // Check for edit form.
        if ($view == 'entry' && $layout == 'edit' && !$this->checkEditId('com_goodpractice.edit.entry', $id))
        {
            // Somehow the person just went to the form - we don't allow that.
            $this->setError(JText::sprintf('JLIB_APPLICATION_ERROR_UNHELD_ID', $id));
            $this->setMessage($this->getError(), 'error');
            $this->setRedirect(JRoute::_('index.php?option=com_goodpractice&view=entries', false));

            return false;
        }

        parent::display();

        return $this;

        /* call parent behavior
          parent::display($cachable, $urlparams);

          // Set the submenu
          GoodpracticeHelper::addSubmenu(JRequest::getCmd('view', $this->default_view));
          return $this; */
    }

}
